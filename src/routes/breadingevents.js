import express from 'express';
import Pet from '../models/Pet';
import BreaderEvent from '../models/BreaderEvent';
import authenticate from '../middlewares/authenticate';
import parseErrors from '../utils/parseError';

const router = express.Router();
router.use(authenticate);

// Creating a Breading Events
router.post('/', (req, res) => {

    const { pets, place, startTime, endTime, status } = req.body;

    const newBreadingEvent = { breederInfo: req.userId, pets, place, startTime, endTime, status };

    BreaderEvent.create(newBreadingEvent, (err, breaderEvent) => {
        if (err) {
			res.status(400).json({ errors: parseErrors(err.errors) });
		}else {
			res.json({ breaderEvent });
		}
    });

});

//Fetching all the breading events associated with a pet
router.get('/', (req, res) => {
    BreaderEvent.find({
        breederInfo: req.userId,
    }).populate('breederInfo', 'local.email local.confirmed').exec((err, breaderEvent) => {
        if (err) {
			res.status(400).json({ errors: parseErrors(err.errors) });
		}else {
			res.json(breaderEvent);
		}
    });

});

//Fetching one breading event associated with pet
router.get('/:brederEventId', (req, res) => {
    BreaderEvent.findOne({
        userId: req.userId,
        _id: req.params.brederEventId
    }).exec((err, breaderEvent) => {
       if (err) {
           res.status(400).json({ errors: parseErrors(err.errors) });
       }else {
           res.json({ breaderEvent });
       }
    });
});

//Delete a breading event 
router.delete('/:id', (req, res) => {
	BreaderEvent.findOne({ 
        _id: req.params.id, 
        userId: req.userId 
    }, (err, breaderEvent) => {
		if(err){
			return res.status(400).json({ errors: parseErrors(err.errors) });
        }
        
        if (breaderEvent === null) {
            return res.status(404).json({ errors: "BreaderEvent not found" });
        }

		breaderEvent.remove(function(err, breaderEvent){
			if(err){
				return res.status(400).json({ errors: parseErrors(err.errors) });
			}
			if(!breaderEvent){
				return res.status(404).json({ errors: "BreaderEvent not found" });
			}
			return res.status(200).json({ message: "Playdate successfully deleted" });
		});
	});
});

//Update a breading event

export default router;