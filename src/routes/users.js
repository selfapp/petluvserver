import express from 'express';
import log4js from 'log4js';
import User from '../models/User';
import authenticate from '../middlewares/authenticate';
import {uploadToS3V2} from '../middlewares/uploadS3V2';
import async from 'async';
import PetBreed from "../models/PetBreed";

const router = express.Router();
router.use(authenticate);

const logger = log4js.getLogger();

// Find All users by geolocation
router.get("/findUserByGeoLocationAndDistance", async (req, res) => {
  logger.info(`[GET] /findUserByGeoLocationAndDistance with params lat = ${req.query.lat} lng = ${req.query.lng} maxDistance=${req.query.maxDistance}`);

  let lat = req.query.lat ? Number(req.query.lat) : 32.928370;
  let lng = req.query.lng ? Number(req.query.lng) : -96.692330;
  const loc = { type: 'Point', coordinates: [lng, lat] };

  let maxDistance = Number(req.query.maxDistance ? req.query.maxDistance : 10 * 10000);
  let distanceMultiplier = 0.00062137;


  // meter to mile conversion 0.00062137
  // 100000 * 0.00062137  = 62 miles
  try {
    let users = await User.aggregate(
      [
        {
          "$geoNear": {
            "near": loc,
            "distanceField": "distance",
            "spherical": true,
            "maxDistance": maxDistance,
            "distanceMultiplier": distanceMultiplier
          }
        },
        { $match: { pushToken : { $ne : null }} },
        { "$sort": { updatedAt: -1 } },
        {
          $project: {
            "distance" : { "$floor": "$distance" },
            "local.userName" : 1,
            "location": 1,
            "pushToken": 1
          }
        }
      ]
    )

    console.log(users.map(user => user.pushToken ));
    return res.status(200).json({ 
      success: true,
      message: `Successfully retrive userlist within ${maxDistance * distanceMultiplier}mi`,
      data: { users }
    })
  } catch (e) {
    logger.error("Failed to retrive user list. Error = " + e);
    return res.status(200).json({ 
      success: false,
      message: `Failed to retrive users list`,
      data: null
    })
  }
})

//Getting a single user
router.get('/:id', async (req, res) => {
  logger.level = 'info';
  logger.info(`[GET] getting user with userId:${req.params.id} loginType: ${req.loginType}`);

  let loginType = req.loginType;
  // let loginType = 'facebook';
  let queryString = `-local.passwordHash`;
  if (loginType === 'local') {
    queryString = `-local.passwordHash -local.confirmationToken -local.forgotPasswordToken -social`;
  } else if (loginType === 'facebook') {
    queryString = `-local -social.fb.token -social.fb.id`;
  } else if (loginType === 'gmail') {
    queryString = `-local -social.gmail.token -social.gmail.id`;
  }

  User.findById(req.params.id).select(queryString).exec(async (err, user) => {
    if (err) {
      logger.error(`Error while retriving user ${e}`);
    }
    if (!user) {
      return res.status(404).json({
        errors: {global: "User Not Found"}
      });
    } else {
      let userObj = user.toObject();
      if(userObj.accountType.breeder.isServiceProvider) {
        userObj.accountType.breeder = await fixResponseBreeds(userObj.accountType.breeder);
      }
      if(userObj.accountType.dogWalker.isServiceProvider) {
        userObj.accountType.dogWalker = await fixResponseBreeds(userObj.accountType.dogWalker);
      }

      if (loginType === 'local') {
        userObj['userName'] = userObj.local.userName;
        userObj['profilePic'] = userObj.local.photo;
      } else if (loginType === 'facebook') {
        userObj['userName'] = userObj.social.fb.userName;
        userObj['profilePic'] = userObj.social.fb.photo;
      } else if (loginType === 'gmail') {
        userObj['userName'] = userObj.social.gmail.userName;
        userObj['profilePic'] = userObj.social.gmail.photo;
      }

      return res.status(200).json(userObj);
    }
  });
});

// To get All the users
router.get('/', async (req, res) => {
  logger.info(`[GET] Fetch All the users`);
  try {
    let users = await User.find().select('-local.passwordHash');
    return res.status(200).json({ 
      success: true,
      data: {users},
      message: "Successfully retrive user list"
    });
  } catch (e) {
    return res.status(400).json({ 
      success: false,
      data: null,
      message: `Could not retrive user list ${e}`
    })
  }
})

// Update user profile
router.put('/profile', async (req, res) => {
  logger.level = 'info';
  logger.info("PUT] profile update url /profile", req.userId);

  let user = req.user;
  let loginType = req.loginType;

  if (req.body.profilePic) {
    let profilePic = req.body.profilePic;
    if (loginType === 'local') {
      user.local.photo = profilePic;
    } else if (loginType === 'facebook') {
      user['social']['fb']['photo'] = profilePic;
    } else if (loginType === 'gmail') {
      user['social']['gmail']['photo'] = profilePic;
    }
  }

  if (req.body.userName) {
    let userName = req.body.userName;
    if(loginType === 'local') {
      user.local.userName = userName;
    } else if (loginType === 'facebook') {
      user['social']['fb']['userName'] = userName;
    } else if (loginType === 'gmail') {
      user['social']['gmail']['userName'] = userName;
    }
  }

  if (req.body.photoId) {
    user.idImage = req.body.photoId
  }

  if (req.body.businessEmail)
    user.businessEmail = req.body.businessEmail;
  if (req.body.phoneNumber)
    user.phoneNumber = req.body.phoneNumber;
  if (req.body.businessUrl)
    user.businessUrl = req.body.businessUrl;
  if (req.body.ssn)
    user.ssn = req.body.ssn;
  if (req.body.address)
    user.address = req.body.address;
  if (req.body.accountType) {
    let breeder = {
      isServiceProvider: req.body.accountType.breeder.isServiceProvider,
      akcNumber: req.body.accountType.breeder.akcNumber,
      businessDescription: req.body.accountType.breeder.businessDescription,
      species: req.body.accountType.breeder.species ? req.body.accountType.breeder.species : [],
      breeds: req.body.accountType.breeder.breeds ? req.body.accountType.breeder.breeds : [],
      akcImage: req.body.accountType.breeder.akcImage
    };

    if(req.body.accountType.breeder.isServiceProvider) {
      breeder = await fixSpecies(breeder);
    } else {
      breeder = {
        isServiceProvider: req.body.accountType.breeder.isServiceProvider,
        akcNumber: null,
        businessDescription: null,
        species: [],
        breeds: [],
        akcImage: null
      };
    }

    let petGroomer = {
      isServiceProvider: req.body.accountType.petGroomer.isServiceProvider,
      businessDescription: req.body.accountType.petGroomer.businessDescription,
      species: req.body.accountType.petGroomer.species,
      businessImages: req.body.accountType.petGroomer.businessImages
    };
    petGroomer = await fixOnlySpecies(petGroomer);

    let petTrainer = {
      isServiceProvider: req.body.accountType.petTrainer.isServiceProvider,
      businessDescription: req.body.accountType.petTrainer.businessDescription,
      species: req.body.accountType.petTrainer.species,
      businessImages: req.body.accountType.petTrainer.businessImages
    };
    petTrainer = await fixOnlySpecies(petTrainer);

    let petSitter = {
      isServiceProvider: req.body.accountType.petSitter.isServiceProvider,
      businessDescription: req.body.accountType.petSitter.businessDescription,
      species: req.body.accountType.petSitter.species,
      businessImages: req.body.accountType.petSitter.businessImages
    };
    petSitter = await fixOnlySpecies(petSitter);

    let dogWalker = {
      isServiceProvider: req.body.accountType.dogWalker.isServiceProvider,
      businessDescription: req.body.accountType.dogWalker.businessDescription,
      breeds: req.body.accountType.dogWalker.breeds,
      businessImages: req.body.accountType.dogWalker.businessImages
    };
    dogWalker = await fixSpecies(dogWalker);

    user.accountType.breeder = {...user.accountType.breeder, ...breeder};
    user.accountType.petGroomer = {...user.accountType.petGroomer, ...petGroomer};
    user.accountType.petTrainer = {...user.accountType.petTrainer, ...petTrainer};
    user.accountType.petSitter = {...user.accountType.petSitter, ...petSitter};
    user.accountType.dogWalker = {...user.accountType.dogWalker, ...dogWalker};
  }

  user.save(err => {
    if (err) {
      logger.error(`Error while saving user info ${err}`);
      return res.status(400).json(err);
    } else {
      return res.status(200).json(user);
    }
  });
});

//Upload user images
router.put("/profile/upload", async (req, res) => {
  logger.info("[PUT] In profile update url /profile/upload userId: ", req.userId);

  let fileObj = null;

  if (req.files != undefined) {
    let file = req.files.file;
    console.log(file);
    fileObj = await uploadToS3V2(file, "users", req.userId);
  }

  if (!fileObj) {
    return res.status(400).json({
      success: false,
      response: {
        message: "Could not upload file."
      }
    })
  }

  return res.status(200).json({
    success: true,
    response: {
      url: fileObj.url,
      message: "Successfullly uploaded file."
    }
  })
})


// User as a service provider. Later move into approprite route.
router.get('/service-provider-details/:providerId', async (req, res) => {
  logger.info(`[GET] route: /service-provider-details/${req.params.providerId} sevriceType: ${req.query.serviceType}`);

  let mapping = {
    petSitter: 'petSitter',
    breeder: 'breeder',
    groomer: 'petGroomer',
    trainer: 'petTrainer',
    walker: 'dogWalker'
  };

  let serviceType = req.query.serviceType;
  let serviceProviderId = req.params.providerId;

  try {
    let serviceProvider = await User.findById({_id: serviceProviderId})
                        .select(`-local.passwordHash -social.fb.id -social.gmail.id -social.fb.token -social.gmail.token`);

    if (!serviceProvider) {
      return res.status(404).json({
        response: {
          success: false,
          message: `Service provider not found in database. Id: ${serviceProviderId}`,
          data: null
        }
      });
    }

    let serviceProviderObj = serviceProvider.toObject();
    // Need to think about how to retrive this
    serviceProviderObj["provider"] = "inAppServiceProvider";
    serviceProviderObj["reviews"] = [];
    serviceProviderObj["address"] = "";
    serviceProviderObj["star"] = 0;
    serviceProviderObj["serviceType"] = serviceType;
    serviceProviderObj["photos"] = [];

    if (serviceProviderObj.local) {
      serviceProviderObj["userName"] = serviceProviderObj.local.userName;
      serviceProviderObj["photo"] = serviceProviderObj.local.photo;
    } else if (serviceProviderObj.social.fb) {
      serviceProviderObj["userName"] = serviceProviderObj.social.fb.userName;
      serviceProviderObj["photo"] = serviceProviderObj.social.fb.photo;
    }

    if (mapping[serviceType] == "petSitter") {
      serviceProviderObj["photos"].push(serviceProvider.accountType.petSitter.businessImages);
      serviceProviderObj["description"] = serviceProvider.accountType.petSitter.businessDescription;
    } else if (mapping[serviceType] === "breeder") {
      serviceProviderObj["photos"].push(serviceProvider.accountType.breeder.akcImage);
      serviceProviderObj["description"] = serviceProvider.accountType.breeder.businessDescription;
    } else if (mapping[serviceType] === "petGroomer") {
      serviceProviderObj["photos"].push(serviceProvider.accountType.petGroomer.businessImages);
      serviceProviderObj["description"] = serviceProvider.accountType.petGroomer.businessDescription;
    } else if (mapping[serviceType] === "petTrainer") {
      serviceProviderObj["photos"].push(serviceProvider.accountType.petTrainer.businessImages);
      serviceProviderObj["description"] = serviceProvider.accountType.petTrainer.businessDescription;
    } else if (mapping[serviceType] === "dogWalker") {
      serviceProviderObj["photos"].push(serviceProvider.accountType.dogWalker.businessImages);
      serviceProviderObj["description"] = serviceProvider.accountType.dogWalker.businessDescription;
    }

    // DTO object to refine data needed to display
    let serviceProviderDto = (
      ({
        userName, description, provider, reviews, photo, star, serviceType, photos, address, phoneNumber, businessUrl
      }) => ({ 
        userName, description, provider, reviews, photo, star, serviceType, photos, address, phoneNumber, businessUrl
      })
    )(serviceProviderObj);

    return res.status(200).json({
      response: {
        success: true,
        message: `Successfully retrived service provider details with Id ${serviceProviderId}`,
        data: serviceProviderDto
      }
    })
  } catch (e) {
    logger.level = "error";
    logger.error(e);

    return res.status(400).json({
      response: {
        success: false,
        message: `Error while looking service provider in databae. Id: ${serviceProviderId}`,
        data: null
      }
    })
  }
});

router.put('/push-token', async (req, res) => {
  logger.level = 'info';
  logger.info(`[PUT] user push token /push-token ${req.body.pushToken}`);
  
  User.findByIdAndUpdate(req.userId, {pushToken: req.body.pushToken}, {new: true}, function (err, user) {
    if (err) {
      return res.status(200).json(err);
    }
    if (!user) {
      return res.status(404).json({
        "errors": "404 - user not found"
      });
    }
    return res.status(200).json(user.pushToken);
  });
});

router.post('/push-token', async (req, res) => {
  User.findByIdAndUpdate(req.userId, {pushToken: req.body.pushToken}, {new: true}, (err, user) => {
  });

  return res.status(200).json(req.body.pushToken);
});

router.get('/push-token', async (req, res) => {
  logger.level = 'info';
  logger.info("GET /push-token.");

  User.findById(req.userId).exec((err, user) => {
    if (err) {
      logger.level = 'error';
      logger.error("Error occoured while look up push token from user.", err);
      return res.status(400).json({
        success: false,
        msg: 'Server Error.',
        response: {
          pushToken: ""
        }
      })
    }

    let pushToken = user.pushToken;

    if (pushToken == null) {
      return res.status(200).json({
        success: false,
        msg: 'Null push token.',
        response: {
          pushToken: ""
        }
      })
    }

    return res.status(200).json({
      success: true,
      msg: 'Use has a push token',
      response: {
        pushToken: pushToken
      }
    })
  })
});

router.put('/save-location', async (req, res) => {
  logger.level = 'info';
  logger.info("[PUT] update user location /location userId: ", req.userId);

  if (req.body.currentLocation) {
    logger.info("[INFO] currentLocation: ", req.body.currentLocation);

    let lat = +req.body.currentLocation.lat;
    let lng = +req.body.currentLocation.lng;

    let loc = { type: 'Point', coordinates: [lng, lat] };
    let location = await User.findByIdAndUpdate(req.userId, {currentLocation: req.body.currentLocation, location: loc }, {new: true});
    if (location) {
      return res.status(200).json({success: true});
    }
    return res.status(400).json({success: false});
  } else {
    return res.status(400).json({success: false});
  }
});

let fixSpecies = (obj) => {
  return new Promise((resolve, reject) => {
    let objClone = JSON.parse(JSON.stringify(obj));
    objClone.breeds = [];
    objClone.species = [];
    let finalBreeds = [];
    let finalSpecies = [];
    async.forEach(obj.breeds, async (breed, callback) => {
      let speciesId = breed.split('-')[0];
      let breedIndex = breed.split('-')[1];
      await PetBreed.findOne({_id: speciesId})
        .exec((err, petBreed) => {
          if (err) {
            console.log(err);
          } else if (petBreed) {
            finalBreeds.push(`${petBreed.breeds[breedIndex - 1]}$${petBreed._id}`);
            finalSpecies.push(petBreed._id.toString());

            callback();
          }
        });
    }, (err) => {
      objClone.breeds = Array.from(new Set(finalBreeds));
      objClone.species = Array.from(new Set(finalSpecies));
      resolve(objClone);
    });
  })
};

let fixOnlySpecies = (obj) => {
  return new Promise((resolve, reject) => {
    let objClone = JSON.parse(JSON.stringify(obj));
    objClone.species = [];
    let finalSpecies = [];
    async.forEach(obj.species, async (speciesId, callback) => {
      await PetBreed.findOne({_id: speciesId})
        .exec((err, petBreed) => {
          if (err) {
            // console.log(err);
          } else if (petBreed) {
            finalSpecies.push(petBreed._id.toString());

            callback();
          }
        });
    }, (err) => {
      objClone.species = Array.from(new Set(finalSpecies));
      resolve(objClone);
    });
  })
};

let fixResponseBreeds = (obj) => {
  return new Promise((resolve, reject) => {
    let objClone = JSON.parse(JSON.stringify(obj));
    let finalBreeds = [];

    //breeds from obj
    async.forEach(obj.breeds, async (breed, callback) => {
      var speciesId = breed.split('$')[1];

      //find breed
      await PetBreed.findOne({_id: speciesId})
        .exec((err, species) => {
          if(err) {
          } else {
            //found breed
            //loop and push to finalBreeds
            async.eachOf(species.breeds, (dbBreed, index, cb) => {
              if(breed.split('$')[0] === dbBreed) {
                finalBreeds.push(`${speciesId}-${index + 1}`);
                cb();
              } else {
                cb();
              }
            }, (err) => {
              callback();
            });
          }
        });
    }, (err) => {
      objClone.breeds = finalBreeds;
      resolve(objClone);
    });
  });
};

export default router;
