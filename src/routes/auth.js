import express from 'express';
import passport from 'passport';
import jwt from 'jsonwebtoken';
import log4js from 'log4js';
import axios from 'axios';
import generator from 'generate-password';
import authenticate from '../middlewares/authenticate';
import {
    sendConfirmationEmail,
    resetEmailConfirmation
} from '../config/mailer';
import {
    check,
    validationResult
} from 'express-validator/check';
import User from '../models/User';
import Role from '../models/Role';
import objToArr from '../utils/objToArrConvetor';
import Service from '../models/Service';
import parseError from '../utils/parseError';

const logger = log4js.getLogger();
logger.level = 'info';

const router = express.Router();
router.use('/resend', authenticate);

//Signin route
router.post("/signin", [
    check('email')
        .isEmail().withMessage('must be an email').trim(),
    check('password', 'Password must be at least six characters long alpha numeric only.')
        .isLength({ min: 6 })
        .matches(/\d/)
], (req, res) => {
    logger.info("[POST] /signin ");

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({
            success: false,
            message: errors.array()[0].msg,
            data: null
        });
    }

    let { email, password} = req.body;

    User.findOne({ 'local.email': email}, function (err, user) {
        if (err) {
            logger.error(`Error while finding user ${err}`);
            return res.status(400).json({
                success: false,
                message: errors.array()[0].msg,
                data: null
            });
        }

        if (!user) {
            return res.status(400).json({
                success: false,
                message: "This email does not exist!. Please create an account.",
                data : null
            });
        }

        if (user && !user.isValidPassword(password)) {
            return res.status(400).json({
                success: false,
                message: "Invalid Credentials!",
                data : null
            });
        }
        
        if (user && user.isValidPassword(password)) {
            return res.status(200).json({
                success: true,
                message: "Login Successfull",
                data : user.toAuthJSON("local")
            });
        }
    });
});

//Signup route
router.post('/signup', [
    check('email')
        .isEmail().withMessage('must be an email').trim()
        .custom(email => {
            return User.findOne({
                'local.email': email
            }).then(user => {
                if (user) {
                    throw new Error('The email is already taken. Please try a different email.');
                }
            });
        }),
    check('password', 'Please provide a valid password.')
        .isLength({min: 6 }).matches(/\d/),
    check('password').exists(),
    check('passwordConfirmation', 'Password did not match.')
        .exists()
        .custom((value, {
            req
        }) => value === req.body.password)
], async (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            success: false,
            message: errors.array()[0].msg,
            data: null
        });
    }

    let role = await Role.findOne({ roleType: "USER" });
    if (!role) {
        logger.error("Could not find role.");

        return res.status(400).json({
            success: false,
            message: "",
            data: null
        });
    } 

    const { email, password } = req.body;

    User.findOne({
        $or: [{
            'local.email': email
        }, {
            'social.fb.email': email
        },
            {
                'social.gmail.email': email
            }]
    }, (err, user) => {
        if (err) {
            return res.status(400).json({
                errors: parseError(err.errors)
            });
        }
        if (user) {
            if (user.local.email !== '' && user.local.email != undefined) {
                return res.status(200).json({
                    user: user.toAuthJSON("local")
                });
            } else {
                user.local.email = email;
                user.role = role._id;
                user.setPassword(password);
                user.setConfirmationToken();

                user.save((err, user) => {
                    if (err) {
                        logger.error(`Error while saving user info ${err}`);
                        return res.status(400).json({
                            success: false,
                            "message": "Could not able to sign up. Please try again!",
                            data: null
                        });
                    } else {
                        sendConfirmationEmail(user);
                        return res.status(200).json({
                            success: true,
                            message: "Successfully signed up. Please confirm your email ang log back in.",
                            data : user.toAuthJSON("local")
                        });
                    }
                });
            }
        }
        if (!user) {
            let newUser = new User();

            newUser.local.email = email;
            newUser.role = role._id;
            newUser.setPassword(password);
            newUser.setConfirmationToken();

            newUser.save(async (err, user) => {
                if (err) {
                    logger.error(`Error while saving user info ${err}`);
                    return res.status(400).json({
                        success: false,
                        "message": "Could not able to sign up. Please try again!",
                        data: null
                    });
                } else {
                    sendConfirmationEmail(user);
                    return res.status(200).json({
                        success: true,
                        message: "Successfully signed up. Please confirm your email ang log back in.",
                        data : user.toAuthJSON("local")
                    });
                }
            });
        }
    });
});

// delete a user
router.post("/delete-user", [
    check('email')
        .isEmail().withMessage('must be an email')
        .trim()
        .normalizeEmail(),
    check('password', 'passwords must be at least 6 chars long and contain one number')
        .isLength({
            min: 6
        })
        .matches(/\d/),
], (req, res) => {

    // This api should be allowed only from localhost
    if (req.host != 'localhost') {
        res.status(400).json({
            error: `This api is only accessible from localhost`
        });
    }

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
            errors: objToArr(errors.mapped())
        });
    }
    let {
        email,
        password
    } = req.body;

    User.deleteOne({
        'local.email': email
    }, function (err, user) {
        if (err) {
            return res.status(400).json({
                errors: Array({
                    global: "invalid Credentials"
                })
            });
        }
        res.status(200).json({
            msg: `User with email ${email} has been deleted.`
        });
    });
});

//facebook login route
router.post("/facebook/token", passport.authenticate('facebook-token'), (req, res) => {
    logger.info("[POST] /facebook/token route");

    if (!req.user) {
        return res.status(400).json({
            success: false,
            data: null,
            message: "Could not authenticate!. Please try again."
        });
    }
    if (req.user) {
        return res.status(200).json({
            success: true,
            message: "Facebook Login Successfull.",
            data: req.user.toAuthJSON("facebook")
        });
    }
});

// Gmail Login route
router.post("/google/token", async (req, res) => {
    logger.info("[POST] /google/token route");

    let {access_token} = req.body;
    let config = {
        headers: {
            "Authorization": `Bearer ${access_token}`
        }
    };
    let userInfoResponse = await axios.get('https://www.googleapis.com/userinfo/v2/me', config);

    let {email, id, given_name, picture} = userInfoResponse.data;

    let query = {$or: [{'local.email': email}, {'social.fb.email': email}, {'social.gmail.email': email}]};

    User.findOne(query, (err, user) => {
        if (err) {
            return res.status(400).json({
                success: false,
                data: null,
                message: "Could not authenticate!. Please try again."
            });
        }
        if (user) {
            if (user.social.gmail.id === id) {
                return res.status(200).json({
                    success: true,
                    message: "Gmail login successful.",
                    data: user.toAuthJSON("gmail")
                });
            } else {
                user.social.gmail.email = email;
                user.social.gmail.token = access_token;
                user.social.gmail.id = id;
                user.social.gmail.photo = picture || '';
                user.social.gmail.userName = given_name || '';

                user.save(err => {
                    if (err) {
                        return res.status(401).json({
                            success: false,
                            message: "Could not authenticate.",
                            data: null
                        });
                    } else {
                        return res.status(200).json({
                            success: true,
                            message: "Gmail login successful.",
                            data: user.toAuthJSON("gmail")
                        });
                    }
                });
            }
        }
        if (!user) {
            let user = new User();

            user.social.gmail.email = email;
            user.social.gmail.token = access_token;
            user.social.gmail.id = id;
            user.social.gmail.photo = picture || '';
            user.social.gmail.userName = given_name || '';

            user.save(err => {
                if (err) {
                    return res.status(400).json({
                        success: false,
                        message: "Could not authenticate.",
                        data: null
                    });
                } else {
                    return res.status(200).json({
                        success: true,
                        message: "Gmail login successful.",
                        data: user.toAuthJSON("gmail")
                    });
                }
            });
        }
    });
});

//Instragram login route
router.get("/instragram/token", passport.authenticate('instagram-token'), (req, res) => {

});

//Email confirmation
router.get('/verify', async (req, res) => {
    logger.level = 'info';
    logger.info(`[GET] verification route /verify?confirmationToken=... `);
    logger.info(`Protocol=${req.protocol} Host=${req.host}`);

    const token = req.query.confirmationToken;

    let HOST = `${process.env.HOST}:${process.env.SERVER_PORT}`;
    if (process.env.PETLUVS_ENV === 'PROD') {
        HOST = `${process.env.HOST}`;
    }

    if ((`${req.protocol}://${req.get('host')}`) == HOST) {
        try {
            let decoded = jwt.verify(token, process.env.JWT_SECRET);
            User.findOneAndUpdate({'local.confirmationToken': token }, {
                'local.confirmationToken': '',
                'local.confirmed': true
            }, {
                new: true
            }).then(user => {
                user 
                    ? res.render('verification_complete', { message: "Verification is complete. Please log in to your app."})
                    : res.render('verification_complete', { message: "Could not verified. Please try again later"})
            });
        } catch (err) {
            res.render('verification_complete', { message: "Your token has expired."})
        }
    } else {
        res.render('verification_complete', { message: "Request is from unknown source."})
    }
});

//Resend email
router.get('/resend', (req, res) => {
    //req.user has user
    User.findOne({
        _id: req.userId
    })
        .then(user => {
            if (user) {
                user.setConfirmationToken();
                user.save((err, user) => {
                    if (err) {
                        return res.status(400).json({
                            errors: parseError(err.errors)
                        });
                    } else {
                        sendConfirmationEmail(user);
                        return res.status(200).json({
                            message: "Email sent"
                        });
                    }
                });
            } else {
                return res.status(400).json({});
            }
        });
});

//Resend email
//send email as query param
router.get('/resendemail', async (req, res) => {
    logger.level = 'info';
    logger.info(`[GET] resend email route /resendemail queryparam email = ${req.query.email}`);

    const email = req.query.email;
    let user  = await User.findOne({ "local.email" : email })
                    .select(`local`);
    if (user) {
        user.setConfirmationToken();
        user.save((err, user) => {
            if (err) {
                return res.status(400).json({
                    errors: parseError(err.errors)
                });
            } else {
                sendConfirmationEmail(user);
                return res.status(200).json({
                    message: "Email sent"
                });
            }
        })
    } else {
        return res.status(400).json({ error: "User not found" })
    }
});

// User lookup for forgot password screen
router.post('/user-lookup', async (req, res) => {
    try {
        if (req.body.email) {
            let user = await User.findOne({'local.email': req.body.email});
            if (user) {
                return res.status(200).json({success: true, message: "Email is found."});
            } else {
                return res.status(400).json({success: false, message: "Could not find the record."});
            }
        } else {
            return res.status(400).json({success: false, message: "Please send valid params!"});
        }
    } catch (e) {
        return res.status(400).json({success: false, message: "User Lookup failed. Please try again!"});
    }
});

// Reset metod selected and generate token and send them token with selected metod
router.post('/send-tokenviamethod', async (req, res) => {
    try {
        if (req.body.email && req.body.requestMethod) {
            let user = await User.findOne({'local.email': req.body.email});
            if (user) {
                let tempPassword = generator.generate({
                    length: 5,
                    numbers: true,
                    uppercase: false,
                    exclude: "Characters"
                })

                user.setForgotPasswodToken(tempPassword);
                user.save((err, user) => {
                    if (err) {
                        return res.status(400).json({success: true, message: "Could not save token."});
                    } else {
                        resetEmailConfirmation(user);
                        return res.status(200).json({
                            success: true,
                            message: "Email is found.",
                            password: user.local.forgotPasswordToken
                        });
                    }
                });
            } else {
                return res.status(400).json({success: false, message: "Could not find the record."});
            }
        } else {
            return res.status(400).json({success: false, message: "Please send valid params!"});
        }
    } catch (e) {
        return res.status(400).json({success: false, message: "User Lookup failed. Please try again!"});
    }
});

// Verify forgot password
router.post('/verify-forgotpass', async (req, res) => {
    try {
        if (req.body.email && req.body.fgPassword) {
            let user = await User.findOne({'local.email': req.body.email});
            if (user) {
                let {forgotPasswordToken} = user.local;
                if (req.body.fgPassword === forgotPasswordToken) {
                    return res.status(200).json({success: true, message: "Password is verified"});
                } else {
                    return res.status(400).json({success: false, message: "Opps Password did not match!"});
                }
            } else {
                return res.status(400).json({success: false, message: "Could not find the record."});
            }
        } else {
            return res.status(400).json({success: false, message: "Please send valid params!"});
        }
    } catch (e) {
        return res.status(400).json({success: false, message: "Verify forgot password failed. Please try again!"});
    }
});

//Reset password
router.post('/resetpassword', async (req, res) => {
    try {
        let password = req.body.password;
        let user = await User.findOne({'local.email': req.body.email});
        if (!user) {
            return res.status(400).json({
                response: {
                  success: false,
                  data: null,
                  msg: 'There is no account exist with provided email.'
                }
            });
        }

        let isPreviousPasswrod = user.isPreviousPassword(password);
        if (isPreviousPasswrod) {
            return res.status(400).json({
                response: {
                  success: false,
                  data: null,
                  msg: 'This password previously used to sign in. Please try a different password.'
                }
            });
        }

        if (user) {
            user.local.confirmed = false;
            user.setPassword(password);
            user.setConfirmationToken();

            user = await user.save();
            if (user) {
                resetEmailConfirmation(user);
                return res.status(200).json({
                    response: {
                      success: true,
                      data: null,
                      msg: 'An email has been sent to you. Please confirm your email and login back.'
                    }
                });
            }

            return res.status(400).json({
                response: {
                  success: false,
                  data: null,
                  msg: 'Could not chagne password. Please try again.'
                }
            });
        }

    } catch (e) {
        console.log(e);
        return res.status(400).json({
            response: {
              success: false,
              data: null,
              msg: 'Could not chagne password.. Please try again.'
            }
        });
    }  
});


export default router;
