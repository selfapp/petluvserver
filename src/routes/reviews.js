import express from 'express';
import {check, validationResult} from 'express-validator/check';
import moment from "moment";
import log4js from 'log4js';
import authenticate from '../middlewares/authenticate';
import parseErrors from "../utils/parseError";
import objToArr from "../utils/objToArrConvetor";

import Playdate from "../models/Playdate";
import Review from "../models/Review";
import Feed from '../models/Feed';
import {queConn} from '../config/kuePortConfig';
import User from '../models/User';

const logger = log4js.getLogger();

let queue = queConn;
const router = express.Router();
router.use(authenticate);

router.get('/v0', (req, res) => {
  Playdate.find({userId: req.userId})
    .select('_id')
    .exec((err, playdates) => {
      let playDateIds = playdates.map(v => v._id);

      Review.where('playdateId')
        .in(playDateIds)
        .sort('-createdAt')
        .populate([
          {
            path: 'userId',
            model: 'User',
            select: 'local.email local.userName profilePic'
          }
        ])
        .exec((err, reviews) => {
          let avgStarCount = reviews.reduce((total, review) => total + review.starCount, 0);

          avgStarCount = avgStarCount / reviews.length;
          res.json({reviews, avgStarCount});
        })
    });
});


// Fetching All review with receiverId as queryParams
router.get('/', async (req, res) => {
  logger.info(`[GET] Fetching all reviews with receiverId /reviews?${req.query.receiverId}`)

  let receiverId = req.query.receiverId;

  try {
    let reviews = await Review.find({ receiverId })
        .populate([
          {
            path: 'reviewerId',
            model: 'User',
            select: 'local.email local.userName local.photo social.fb.userName social.fb.photo social.gmail.photo social.gmail.userName'
          }
        ]).sort('-createdAt');
    let reviewsMod = reviews.map(review => {
      let reviewObj = review.toObject();
      let userName = "";
      let profilePic = null;

      if (reviewObj.reviewerId.local) {
        userName = reviewObj.reviewerId.local.userName;
        profilePic = reviewObj.reviewerId.local.photo;
      } else {
        if (reviewObj.reviewerId.social.fb) {
          userName = reviewObj.reviewerId.social.fb.userName;
          profilePic = reviewObj.reviewerId.social.fb.photo;
        } else {
          userName = reviewObj.reviewerId.social.gmail.userName;
          profilePic = reviewObj.reviewerId.social.gmail.photo;
        }
      }

      return {
        ...reviewObj,
        reviewerId: {
          userName,
          profilePic
        }
      }
    });

    let avgStarCount = reviewsMod.reduce((total, review) => total + review.starCount, 0);
    avgStarCount = avgStarCount / reviewsMod.length;

    return res.status(200).json({ 
      success: true,
      message: "Successfully fetched review list",
      data: { reviews: reviewsMod, avgStarCount }
    });
  } catch (e) {
    logger.error("Error while retriving review list" + e);
    return res.status(400).json({
      success: false,
      message: "Error while retriving review list",
      data: null
    })
  }
});


//New Review latest -> Creating a Review
router.post('/', [
  check('starCount', 'Must be a number greater than 0 and less than or equal to 5').isInt({min: 1, max: 5}),
  check('description', 'Must be at least 5 characters long and smaller than 300 characters').isLength({
    min: 0,
    max: 300
  })
], async (req, res) => {
  logger.level = 'info';
  logger.info(`[POST] create review`);

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      success: false,
      message: "Validation Failed",
      data: errors.mapped()
    });
  }

  let { starCount, description, receiverId, reviewFor, dataId } = req.body;
  let reviewerId = req.userId;

  let reviewData = {
    starCount, 
    description, 
    receiverId, 
    reviewerId,
    reviewFor,
    dataId,
    userId: req.userId
  }

  console.log(reviewData);
  try {
    let review = new Review(reviewData);
    await review.save();

    await Playdate.findOneAndUpdate({ _id: dataId }, { $push: { reviews: review._id }}, { new: true} );
    
    await User.findOneAndUpdate({ _id: receiverId }, { $push: { reviews: review._id }}, { new: true} );

    let job = queue.create('playDate-review-added', {
      id: review._id,
      playDateId: review.dataId
    })
      .delay(moment().add(5, 'seconds').toDate())
      .save((err) => {
        if (!err) console.log(job.id);
      });


    return res.status(200).json({
      "success": true,
      "message": "Successfully created review",
      "data": review
    })
  } catch (e) {
    logger.level = 'error';
    logger.error("Error while creating Review" + e);
    return res.status(400).json({
      "success": false,
      "message": "Error while creating review",
      "data": null
    })
  }
})

// New review
router.post('/:id', [
  check('starCount', 'Must be a number greater than 0 and less than or equal to 5').isInt({min: 1, max: 5}),
  check('description', 'Must be at least 5 characters long and smaller than 300 characters').isLength({
    min: 0,
    max: 300
  })
], (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({errors: objToArr(errors.mapped())});
  }

  Playdate.findById(req.params.id)
    .exec((err, playdate) => {
      if (err) {
        return res.status(400).json({errors: parseErrors(err.errors)});
      }
      if (playdate) {
        let {starCount, description} = req.body;
        let userId = req.userId;
        let playdateId = req.params.id;
        let reviewFor = 'playdate';
        let receiverId = playdate.userId;
        let params = {
          starCount, description, userId, playdateId, reviewFor,receiverId
        };

        Review.create(params, async (err, review) => {
          if (err) {
            return res.status(400).json({errors: parseErrors(err.errors)});
          } else {
            let user = await User.findOneAndUpdate({ _id: playdate.userId }, { $push: { reviews: review._id }}, { new: true} );

            let job = queue.create('playDate-review-added', {
              id: review._id,
              playDateId: review.playdateId
            })
              .delay(moment().add(5, 'seconds').toDate())
              .save((err) => {
                if (!err) console.log(job.id);
              });
            res.json({review: review});
          }
        });
      }
    });
});

// Reviews for Playdate ID
router.get('/:id', (req, res) => {
  Playdate.findOne({_id: req.params.id})
    .exec((err, playdate) => {
      if (err) return res.status(422).json({errors: err.errors});

      if (!playdate) return res.status(422).json({errors: ['Playdate not found']});

      if (playdate) {
        let query;
        if (playdate.userId.toString() === req.userId.toString()) {
          query = {
            playdateId: req.params.id
          };
        } else {
          query = {
            playdateId: req.params.id,
            verified: true
          }
        }
        Review.where(query)
          .populate(
            {
              path: 'userId',
              select: 'local.userName profilePic'
            }
          )
          .exec((err, reviews) => {
            if (err) {
              return res.status(422).json({errors: err});
            } else {
              return res.json({reviews: reviews});
            }
          });
      }
    });
});

// Accept review
router.post('/accept/:id', (req, res) => {
  let query = Review.where({_id: req.params.id});
  query.findOne((err, review) => {
    if (err) return res.status(422).json({errors: err});
    if (review) {
      review.verified = true;
      review.save((err, review) => {
        if (err) return res.status(422).json({errors: err});
        if (review) {
          res.json({review});
        }
      });
    }
  });
});

// Decline review
router.delete('/decline/:id', (req, res) => {
  let query = Review.where({_id: req.params.id});
  query.findOne((err, review) => {
    if (err) return res.status(422).json({errors: err});
    if (review) {
      if (!review.verified) {
        review.remove();
        res.json({status: true});
      } else {
        res.status(422).json({status: false, message: "Can't remove accepted review"});
      }
    }
  });
});

// Share a review on your feed
router.post('/share/:id', (req, res) => {
  logger.level = 'info';
  logger.info(`[GET] /reviews/share/${req.params.id}`);
    
  Review.findOne({_id: req.params.id}).exec((err, review) => {
    if (err) {
      return res.status(422).json({errors: err});
    }

    Feed.create({
      category: 2,
      record: req.params.id,
      onModel: 'Review'
    });
    res.json({status: true});
  });
});

export default router;
