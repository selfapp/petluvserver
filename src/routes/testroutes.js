import express from 'express';
import Pet from '../models/Pet';
import AWS from 'aws-sdk';
import Expo from 'expo-server-sdk';
import Service from '../models/Service';
import Review from '../models/Review';
import Role from '../models/Role';
import log4js from 'log4js';

import Location from '../models/Location';
const router = express.Router();
// router.use(authenticate);
import { uploadToS3V2 } from '../middlewares/uploadS3V2';

let expo = new Expo();

const logger = log4js.getLogger();

const BUCKET_NAME = 'petluvs';
const IAM_USER_KEY = 'AKIAJMOK5SG3XI37FOJQ';
const IAM_USER_SECRET = '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';
import {pushNotification} from '../services/pushNotification';
import User from '../models/User';
import Playdate from "../models/Playdate";

// Creating a pet, uploadLocal can be used as a middleware for the file
router.post('/', (req, res) => {
    let file = req.files.file;
    uploadToS3(file, req, res);

});

router.get('/testlogger', (req, res) => {
  logger.level = 'error';
  logger.error("This is a test logger");
  res.json({ success: true })
})

// Testing a review
router.post('/review', async (req, res) => {
  let review = {
      starCount: 0,
      description: "This is a dummy review",
      reviewFor: 'non',
      userId: req.userId,
      playdateId: null
  };

  try {
    let reviewRes = await Review.create( review );
    console.log(review)
    return res.json({
      reviews: reviewRes
    });
  } catch (error) {
    console.log('Error creating ')
    return res.status(400).json({
      "errors": error
    })
  }
});



function uploadToS3(file, req,res) {
    let s3bucket = new AWS.S3({
      accessKeyId: IAM_USER_KEY,
      secretAccessKey: IAM_USER_SECRET,
      Bucket: BUCKET_NAME
    });
    s3bucket.createBucket(function () {
        var params = {
          Bucket: BUCKET_NAME,
          Key: file.name,
          Body: file.data
        };
        s3bucket.upload(params, function (err, data) {
          if (err) {
            console.log('error in callback');
            console.log(err);
            return res.json({ msg: 'error'});
          }
          console.log('success');
          console.log(data);
          return res.json({ msg: success});
        });
    });
  }

  router.get('/send-notification', async (req, res) => {
    logger.level = 'info';
    logger.info("send notification test route /send-notification");
    const pushTokens = await User.find({pushToken: {$ne: null}});
    // console.log(pushTokens);
    let tokenArr = pushTokens.map(el => {
        return el['pushToken']
    });
    console.log(tokenArr);
    let pushNotRes = await pushNotification(tokenArr);

    return res.status(200).json({
      success: true
    })
  })

router.post('/uploadtos3v2', async (req, res) => {
  if (req.files != undefined) {
    let file = req.files.file;
    let fileObj = await uploadToS3V2(file, "pets", "5ce51d2ac4c27c8bd70dd76f");
  }
  return res.json({ routeName: "uploadtos3v2" })
})

router.post('/uploadvideotest', async (req, res) => {
  console.log(req.files);
  if (req.files != undefined) {
    let file = req.files.file;
    let fileObj = await uploadToS3V2(file, "pets", "5ce51d2ac4c27c8bd70dd76f");
  }
  return res.json({ routeName: "uploadvideotest" })
})

router.get('/app-service-provider-list', async (req, res) => {
  let mapping = {
    petSitter: 'petSitter',
    breeder: 'breeder',
    groomer: 'petGroomer',
    trainer: 'petTrainer',
    walker: 'dogWalker'
  };

  let requestType = req.query.type;
  console.log(requestType);

  let user = await User.find({[`accountType.${mapping[requestType]}.isServiceProvider`]: true })
                  .select(
                    `local.userName businessUrl 
                    currentLocation phoneNumber 
                    accountType.${mapping[requestType]}.businessDescription 
                    accountType.${mapping[requestType]}.businessImages`
                  );
  return res.status(200).json({
    success: user
  })
})


// Review calculation
router.get('/reviews', async (req, res) => {
  let reviews = await Review.find({});
  
  return res.status(200).json({ res: "reviews"})
})

// Geo calculator
router.get('/findPlacesbyGeoLocation', async (req, res) => {
  let lat = 32.928370;
  let lng = -96.692330;
  const loc = { type: 'Point', coordinates: [lng, lat] };
  let results;
  Location.aggregate(
    [
      {
        "$geoNear": {
          "near": loc,
          "distanceField": "distance",
          "spherical": true,
          "maxDistance": 10* 10000,
          "distanceMultiplier": 0.00062137
        }
      }
      // {
      //   $sort : { star: 1 }
      // }
    ], 
    function(err, results) {
      if (results) {
        results = results;
        return res.status(200).json({ res: results })
      }
    }
  )
})

// Update user location
router.put('/updatePlaceLocationObject', async (req, res) => {
  let updatedLocArr = [];

  let locArr = await Location.find();
  if (locArr) {
    await Promise.all(locArr.map(async l => {
      let lat = l.lat;
      let lng = l.lng;
      let locationId = l._id;

      let loc = { type: 'Point', coordinates: [lng, lat] };
      let updatedLoc = await Location.findByIdAndUpdate(locationId, {location: loc }, {new: true});
      updatedLocArr.push(updatedLoc);
    }));
    return res.json({ res: updatedLocArr });
  }else {
    return res.json({ res: "error" });
  }
  // let locObj = await Location.findById(locationId);
  // if (locObj) {
  //   lat = locObj.lat;
  //   lng = locObj.lng;
  // }

  // const loc = { type: 'Point', coordinates: [lng, lat] };
  // console.log(loc);

  // let updatedLocationObj = await Location.findByIdAndUpdate(locationId, {location: loc }, {new: true});
})

// Update user location
router.put('/updateUserLocationObj', async (req, res) => {
  let updatedUsers = [];

  let users = await User.find();
  if (users) {
    await Promise.all(users.map(async u => {
      let lat = u.currentLocation.lat;
      let lng = u.currentLocation.lng;
      let userId = u._id;

      let loc = { type: 'Point', coordinates: [lng, lat] };
      let updatedUser = await User.findByIdAndUpdate(userId, {location: loc }, {new: true});
      updatedUsers.push(updatedUser);
    }));
    return res.json({ res: updatedUsers });
  }else {
    return res.json({ res: "error" });
  }
})

// Geo calculator for user
router.get('/findUserByGeoLocation', async (req, res) => {
  let lat = 32.928370;
  let lng = -96.692330;
  const loc = { type: 'Point', coordinates: [lng, lat] };
  let results;
  // meter to mile conversion 0.00062137
  // 100000 * 0.00062137  = 62 miles
  User.aggregate(
    [
      {
        "$geoNear": {
          "near": loc,
          "distanceField": "distance",
          "spherical": true,
          "maxDistance": 10 * 10000,
          "distanceMultiplier": 0.00062137
        }
      },
      {
        $project: {
          "distance" : { "$floor": "$distance" },
          "local.userName" : 1,
          "location": 1
        }
      }
    ], 
    function(err, results) {
      if (results) {
        results = results;
        return res.status(200).json({ res: results })
      }
    }
  )
})

// Creating roles
router.post('/role', async (req, res) => {

  try {
    let role = await Role.create({});
    if (role) {
      return res.json({ res: role});
    } else {
      return res.json({ res: "Could not create a role"});
    }
  } catch(e) {
    console.log(e);
    return res.json({ res: "Internal server error" });
  }
})

// Role Lookup by name
router.post('/findByRoleName', async (req, res) => {
  let roleName = req.body.roleName;
  let role = await Role.findOne({ roleType: roleName });

  if(role) {
    return res.json({res : role })
  }
  return res.json({ res : "No roule found with the name" })
})

// Update all user roles
router.put('/updateAllUserRoles', async (req, res) => {
  let updatedUsers = [];
  let roleName = req.body.roleName;

  let role = await Role.findOne({ roleType: roleName });

  let users = await User.find();
  if (users) {
    await Promise.all(users.map(async u => {
      let userId = u._id;
      let updatedUser = await User.findByIdAndUpdate(userId, {role: role._id }, {new: true});
      updatedUsers.push(updatedUser);
    }));
    return res.json({ res: updatedUsers });
  }else {
    return res.json({ res: "error" });
  }
})

router.get('/testejspage', (req, res) => {
  res.render('verification_complete', { message: "Verification is complete. Please log in to your."});
})

export default router;
