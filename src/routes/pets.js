import express from 'express';
import {check, validationResult} from 'express-validator/check';
import validator from 'validator';
import log4js from 'log4js';

import Pet from '../models/Pet';
import User from '../models/User';
import Media from '../models/Media';
import authenticate from '../middlewares/authenticate';
import parseErrors from '../utils/parseError';
import {uploadToS3} from '../middlewares/uploadS3';
import objToArr from '../utils/objToArrConvetor';
import {uploadToS3V2} from '../middlewares/uploadS3V2';

const logger = log4js.getLogger();

const router = express.Router();
router.use(authenticate);

// Creating a pet
router.post('/', [
    check('name', 'Must be 3 or greater')
      .isLength({min: 3}),
    check('species', 'Must be 3 or greater')
      .isLength({min: 3}),
    check('breed', 'Must be 3 or greater')
      .isLength({min: 3})
  ],
  async (req, res) => {
    logger.info("[POST] /pets/ creating pet");

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({errors: objToArr(errors.mapped())});
    }

    let fileObj = null;

    let {name, species, breed, birthday, insured, fixed, fullyVaccinated} = req.body;
    let userId = req.userId;

    let newPet = {
      userId,
      name,
      species,
      breed,
      birthday,
      attributes: {
        insured,
        fixed,
        fullyVaccinated
      },
      imagePath: fileObj !== null ? fileObj : {imageName: '', url: ''}
    };

    Pet.create(newPet, (err, pet) => {
      if (err) {
        logger.error(`Error while create new pet ${err}`);
        return res.status(400).json({errors: parseErrors(err.errors)});
      } else {
        User.findByIdAndUpdate(userId, {$push: {pets: pet._id}}, async (err, user) => {
          if (err) {
            logger.error(`Error while user pets ${err}`);
            return res.status(400).json({errors: parseErrors(err.errors)});
          }
          if (req.files != undefined) {
            let file = req.files.file;
            fileObj = await uploadToS3V2(file, "pets", pet._id);
          }

          pet["imagePath"] = fileObj ? fileObj : {imageName: null, url: null}
          pet.save();

          return res.status(200).json(pet);
        });
      }
    });
  });

//Adding pet image
router.put('/:id/addimage', async (req, res) => {
  let fileObj = null;

  if (req.files != undefined) {
    let file = req.files.file;
    fileObj = await uploadToS3(file);
  }

  if (fileObj == null) {
    return res.status(400).json({errors: "You sent a null pet "});
  }

  Pet.find({
    _id: req.params.id,
    userId: req.userId
  }).exec((err, pet) => {
    if (err) {
      return res.status(400).json({errors: parseErrors(err.errors)});
    }
    if (!pet) {
      return res.status(404).json({errors: "Pet not found"});
    }

    pet[0].imagePath.push(fileObj);
    pet[0].save((err, pet) => {
      if (err) {
        return res.status(400).json({errors: parseErrors(err.errors)});
      }
      return res.status(200).json(pet);
    });
  });
});

// Fetching all the userrelated pets, can populate only specific fied
router.get('/', (req, res) => {
  Pet.find({userId: req.userId}).populate('userId', 'local.email').exec((err, pets) => {
    if (err) {
      return res.status(400).json({errors: parseErrors(err.errors)});
    }
    return res.status(200).json(pets);
  });
});

//Fetching pet with pet Id and user Id
router.get('/:petId', (req, res) => {
  logger.level = 'info';
  logger.info(`[GET] get pet details /id petId = ${req.params.petId}`);

  Pet.findOne({
    _id: req.params.petId
  })
  .populate({
    path: 'media',
    model: 'Media',
  })
  .exec((err, pet) => {
    if (err) {
      return res.status(400).json({errors: parseErrors(err.errors)});
    }
    if (!pet) {
      return res.status(404).json({errors: "Pet not found"});
    }
    return res.status(200).json(pet);
  });
});

/**
 * Delete a Pet. Need to send id
 */
router.delete('/:id', (req, res) => {
  let userId = req.userId;

  Pet.findOne({
    _id: req.params.id,
    userId
  }, (err, pet) => {
    if (err) {
      return res.status(400).json({errors: parseErrors(err.errors)});
    }

    if (pet === null) {
      return res.status(404).json({errors: "Pet not found"});
    }

    pet.remove(function (err, pet) {
      if (err) {
        return res.status(400).json({errors: parseErrors(err.errors)});
      }
      if (!pet) {
        return res.status(404).json({errors: "Pet not found"});
      }
      User.findByIdAndUpdate(userId, {$pull: {pets: pet._id}}, (err, user) => {
        if (err) {
          return res.status(400).json({errors: parseErrors(err.errors)});
        }
        return res.status(200).json({message: "Pet successfully deleted"});
      });
    })
  })
});

//Updating a pet
router.put('/:id', [
  check('name', 'Must be 3 or greater')
    .isLength({min: 3})
], (req, res) => {

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({errors: objToArr(errors.mapped())});
  }

  let petId = req.params.id;
  let {name, species, breed, birthday, attributes} = req.body;
  let updatedPet = {
    // userId: req.userId,
    name,
    species: species ? species : 'Cat',
    breed: breed ? breed : 'breed1',
    birthday: birthday ? birthday : '01-01-2018',
    attributes
  };

  Pet.findByIdAndUpdate(
    petId,
    updatedPet,
    {new: true}
    , (err, pet) => {
      if (err) {
        return res.status(400).json({errors: parseErrors(err.errors)});
      }
      return res.status(200).json(pet);
    })
});

router.put('/:id/uploadmedia', async (req, res) => {

  let fileObj = null;
  let petId = req.params.id;
  let mediaType = "image";

  let pet = await Pet.findOne({_id: petId, userId: req.userId});
  if (!pet) {
    console.log("Pet not found with id: " + petId);
  }

  if (req.files != undefined) {
    let file = req.files.file;
    if (req.files.file.mimetype.includes("video"))
      mediaType = "video";
    else if (req.files.file.mimetype.includes("image"))
      mediaType = "image";
    fileObj = await uploadToS3V2(file, "pets", petId);
  }

  if (!fileObj)
    return res.status(400).json({"errors": "Could not uploaded to s3. File Object is null."});
  //Test url "https://petluvsstore.s3.amazonaws.com/pets/5ce51d2ac4c27c8bd70dd76f/IMG_2606.MOV"
  // let isMediaExist = pet.media.find(m => m.url === fileObj.url);
  // if (!isMediaExist) {
    let mediaObj = {
      mediaType,
      url: fileObj.url,
      mediaName: fileObj.imageName,
      petId
    }

    let media = await Media.create(mediaObj);
    if (!media) {
      return res.status(400).json("Could not create media.");
    }
  // }
  pet.media.push(media._id);

  pet.save(err => {
    if (err) {
      console.log(err);
      return res.status(400).json(err);
    }
    pet.populate({
      path: 'media'
    }, (err, ppet) => {
      if (err)
        return res.status(400).json(err);
      return res.status(200).json(ppet);
    })
  })
});

export default router;
