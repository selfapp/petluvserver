import express from 'express';
import async from 'async';
import geolib from 'geolib';
import log4js from 'log4js';

import User from '../models/User';
import Playdate from '../models/Playdate';
import BreaderEvent from '../models/BreaderEvent';
import Feed from '../models/Feed';
import Media from '../models/Media';

import authenticate from '../middlewares/authenticate';

const logger = log4js.getLogger();
const router = express.Router();
router.use(authenticate);

router.post('/share', async (req, res) => {
    logger.level = 'info';
    logger.info(`[POST] /media/ mediaId: ${req.body.mediaId}`);

    let mediaId = req.body.mediaId;

    try {
        let media = await Media.findOne({ _id: mediaId });
        if (media) {
            let feed = await Feed.create({ category: 4, record: mediaId, onModel: 'Media' });
            return res.status(200).json({ res: feed });
        } 
        return res.status(400).json({ res: "error while share media"});
    } catch (e) {
        return res.status(400).json({ res: "error while share media"});
    }
});

export default router;