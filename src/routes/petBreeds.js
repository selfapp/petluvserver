import express from 'express';
import async from 'async';

import PetBreed from '../models/PetBreed';
import authenticate from "../middlewares/authenticate";

const router = express.Router();
router.use(authenticate);

router.get('/', async (req, res) => {
    PetBreed.find({}).sort('speciesName').exec((err, petBreeds) => {
        let result = [];
        let count = 1;
        async.forEach(petBreeds, (petBreed) => {
            count = 1;
            result.push({name: petBreed.speciesName, id: petBreed._id, children: []});
            async.forEach(petBreed.breeds, (breed) => {
                result[result.length - 1].children.push({
                    id: `${petBreed._id}-${count}`,
                    name: breed
                });
                count += 1;
            });
        });

        return res.json(result);
    });
});

export default router;
