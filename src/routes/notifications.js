import express from 'express';
import Notification from '../models/Notification';
import log4js from 'log4js';

import authenticate from "../middlewares/authenticate";
import parseErrors from "../utils/parseError";

const logger = log4js.getLogger();
const router = express.Router();
router.use(authenticate);

router.get('/', async (req, res) => {
    logger.level = 'info';
    logger.info(`[GET] All Notifications /notifications/`);
    
    let loginType = req.loginType;

    try {
        let notifications = await Notification.aggregate([
            { $match : { "userId": req.userId, "read": false } },
            { $sort : { "createdAt": -1 }},
            { 
                $lookup: {
                    "from": "users",
                    "localField": "userId",
                    "foreignField": "_id",
                    "as": "userId"
                }
            },
            { $unwind: "$userId" },
            // { $addFields: {
            //     user: {
            //         userName: `$userId.${loginType}.userName`,
            //         photo: `$userId.${loginType}.photo`
            //     }
            // }},
            {
                $project: {"userId": 0}
            }
        ]);

        return res.status(200).json({notifications});
    } catch (e) {
        logger.level = 'error';
        logger.error(`Failed to retrive notifications `);

        return res.status(400).json({
            errors: "Could not retrive notifications"
        })
    }
});

router.delete('/', (req, res) => {
    Notification.deleteMany({ userId: req.userId }, (err) => {
        if(err) {
            return res.status(400).json({ errors: parseErrors(err.errors) });
        }

        return res.json({status: true});
    });
});

router.post('/read/:id', (req, res) => {
    Notification.findOne({_id: req.params.id, userId: req.userId}, (err, notification) => {
        if(err) {
            return res.status(400).json({ errors: parseErrors(err.errors) });
        }

        if(notification.userId.toString() !== req.userId.toString()) {
            return res.json({status: false});
        }

        Notification.deleteOne({_id: notification._id});
        return res.json({status: true});
    });
});

export default router;
