import express from 'express';
import async from 'async';
import geolib from 'geolib';
import log4js from 'log4js';

import User from '../models/User';
import Playdate from '../models/Playdate';
import BreaderEvent from '../models/BreaderEvent';
import Feed from '../models/Feed';

import authenticate from '../middlewares/authenticate';

const logger = log4js.getLogger();
const router = express.Router();
router.use(authenticate);

router.get('/feeds', async (req, res) => {
    logger.level = 'info';
    logger.info(`[GET] homepage route /feeds page=${req.query.page} perPage=${req.query.perPage}`);
    
    const {page, perPage} = req.query;

    let options = {
        page: parseInt(page) || 1,
        limit: parseInt(perPage) || 10,
        populate: [
            {
                path: 'record',
                select: 'userId receiverId location pets startTime endTime place starCount description url mediaType',
                populate: {
                    path: 'userId pets location playdateId petId',
                    select: 'local.email local.userName profilePic photos lat lng name locName imagePath birthday species breed',
                    populate: {
                        path: 'pets',
                        select: 'name'
                    }
                }
            }
        ],
        sort: {
            'updatedAt': -1
        }
    };

    let feeds = await Feed.paginate({}, options);

    let pages = feeds.pages;

    let startLoc = {
        latitude: 0,
        longitude: 0
    };

    let endLoc = {
        latitude: 0,
        longitude: 0
    };

    let distanceInMi = 0;

    User.findOne(req.userId, (err, user) => {
        if (err) {
            console.log(err)
        }
        if (user) {
            if (user.currentLocation && user.currentLocation.lat && user.currentLocation.lng) {
                let {lat, lng} = user.currentLocation;
                startLoc['latitude'] = lat;
                startLoc['longitude'] = lng;
            }

            let homeData = JSON.parse(JSON.stringify(feeds)).docs.map((feed) => {
                if (feed.record.location && feed.record.location.lat && feed.record.location.lng) {
                    let {lat, lng} = feed.record.location;
                    endLoc['latitude'] = lat;
                    endLoc['longitude'] = lng;
                }
                const distance = geolib.getDistance(startLoc, endLoc);
                distanceInMi = geolib.convertUnit("mi", distance, 1);
                if (feed.record.location) {
                    feed.record.location['distance'] = `${distanceInMi} mi`;
                }
                return feed;
            });

            return res.status(200).json({
                success: true,
                data: homeData,
                pages
            });
        }
    });
});

//Fetching pet with pet Id and user Id
router.get('/', async (req, res) => {
    const {page, perPage} = req.query;

    let options = {
        page: parseInt(page) || 1,
        limit: parseInt(perPage) || 5,
        sort: {'createdAt': -1},
        populate: {
            path: 'userId breederInfo pets location',
            select: 'local.email profilePic name imagePath attributes birthday lat lng locName description placeId star hours photos',
        }
    };
    let totalCount = 0;
    let totalPlaydateRes = await Playdate.find();
    if (totalPlaydateRes) {
        totalCount = totalPlaydateRes.length;
    }

    let startloc = {
        latitude: 0,
        longitude: 0
    };

    let endloc = {
        latitude: 0,
        longitude: 0
    };

    let distanceinMi = 0;

    async.concat([Playdate, BreaderEvent], (model, callback) => {
        //var query = model.find({}).sort({ "createdAt": -1}).limit(10);
        //console.log(query);
        model.paginate({}, options, function (err, docs) {
            if (err) throw err;
            callback(err, docs);
        });

        // query.exec((err, docs) => {
        //     if (err) throw err;
        //     callback(err, docs);
        // });
    }, (err, result) => {
        if (err) res.status(400).json({
            errors: Array({
                global: "Server Error"
            })
        });
        // let resultArr = [...result[0].docs, ...result[1].docs];
        let resultArr = [...result[0].docs];
        let homeDataObj = resultArr.map(obj => obj.toObject());
        let sortedArr = homeDataObj.sort((a, b) => {
            return new Date(b.updatedAt) - new Date(a.updatedAt);
        });

        User.findOne(req.userId, (err, user) => {
            if (err) {
                console.log(err)
            }
            if (user) {
                if (user.currentLocation && user.currentLocation.lat && user.currentLocation.lng) {
                    let {lat, lng} = user.currentLocation;
                    startloc['latitude'] = lat;
                    startloc['longitude'] = lng;
                }

                let homeData = sortedArr.map((playdateObj) => {
                    if (playdateObj.location && playdateObj.location.lat && playdateObj.location.lng) {
                        let {lat, lng} = playdateObj.location;
                        endloc['latitude'] = lat;
                        endloc['longitude'] = lng;
                    }
                    const distance = geolib.getDistance(startloc, endloc);
                    distanceinMi = geolib.convertUnit("mi", distance, 1);
                    if (playdateObj['location']) {
                        playdateObj['location']['distance'] = `${distanceinMi} mi`;
                    }
                    return playdateObj;
                });

                return res.status(200).json({
                    totalCount,
                    success: true,
                    data: homeData
                });
            }
        });
    });
});

export default router;
