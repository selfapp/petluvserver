import express from 'express';
import log4js from 'log4js';
import parseError from '../utils/parseError';
import authenticate from '../middlewares/authenticate';
import Service from '../models/Service';
import User from '../models/User';
import {serviceRequestNotification} from '../services/pushNotification';

const router = express.Router();
router.use(authenticate);

const logger = log4js.getLogger();

/*
* category:
*   sitter
*   breeder
*   groomer
*   trainer
*   walker
* */
router.post('/search', (req, res) => {
    logger.level = 'info';
    logger.info("In /search route for service fetch");
    let mapping = {
        sitter: 'sitter',
        breeder: 'breeder',
        groomer: 'petGroomer',
        trainer: 'petTrainer',
        walker: 'dogWalker'
    };
    let requestType = req.body.category;

    let params = null;
    let breed, serviceType;
    if(requestType === 'sitter'){
        params = {breed: req.body.breed, durationType: req.body.durationType, duration: req.body.duration, comments: req.body.comments};
        breed = req.body.breed;
        serviceType = 'pet sitter';
    } else if(requestType === 'breeder' || requestType === 'trainer'){
        params = {breed: req.body.breed, comments: req.body.comments};
        breed = req.body.breed;
        if(requestType === 'breeder'){
            serviceType = 'pet breeder';
        } else if(requestType === 'trainer'){
            serviceType = 'pet trainer';
        }
    } else if(requestType === 'groomer'){
        params = {breed: req.body.breed, service: req.body.service, comments: req.body.comments};
        breed = req.body.breed;
        serviceType = 'pet groomer';
    } else if(requestType === 'walker'){
        params = {durationType: req.body.durationType, duration: req.body.duration, comments: req.body.comments};
        breed = 'dog';
        serviceType = 'dog walker';
    }

    let notification = `A nearby ${breed} is looking for a ${serviceType}. Respond quickly so you can score this gig.`;
    let popup = `Pet ${breed} is looking for ${serviceType} and commented ${params.comments}`;
    params['userId'] = req.userId;

    if(params){
        Service.create(params, (err, service) => {
            if(err) {
                return res.status(400).json({errors: parseError(err.errors)})
            } else {
                User.find({[`accountType.${mapping[requestType]}.isServiceProvider`]: true, pushToken: {$ne: null}})
                    .select('pushToken')
                    .exec((err, users) => {
                        let tokens = [];
                        users.forEach(user => {
                            tokens.push(user.pushToken);
                        });
                        serviceRequestNotification(service.id, tokens, notification, popup);
                    });
                return res.json(service);
            }
        });
    }
});

export default router;
