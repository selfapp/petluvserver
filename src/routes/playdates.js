import express from 'express';
import {validationResult} from 'express-validator/check';
import geolib from 'geolib';
import moment from 'moment';
import log4js from 'log4js';

import Playdate from '../models/Playdate';
import Location from '../models/Location';
import User from '../models/User';
import Feed from '../models/Feed';
import Pet from '../models/Pet';

import authenticate from '../middlewares/authenticate';
import parseErrors from '../utils/parseError';
import objToArr from '../utils/objToArrConvetor';
import {pushNotification} from '../services/pushNotification';
import {createPlace} from './places';

import {queConn} from '../config/kuePortConfig';

const router = express.Router();
router.use(authenticate);
let queue = queConn;
const logger = log4js.getLogger();

// Max distance to send playdate creation notification.
const maxDistance = 100000;
let distanceMultiplier = 0.00062137;
// Right now 100000*0.00062137 = 62 miles

// Fetching all the userrelated playdates, can populate only specific field
router.get('/', (req, res) => {
  Playdate.find({userId: req.userId})
    .sort('-createdAt')
    .populate([{
      path: 'userId',
      model: 'User',
      select: 'local.email local.userName currentLocation photo'
    }, {
      path: 'pets',
      model: 'Pet',
      select: 'imagePath name birthday'
    }, {
      path: 'location',
      model: 'Location'
    }]).exec((err, playdates) => {
    if (err) {
      console.log(err);
      return res.status(400).json({errors: parseErrors(err.errors)});
    }

    let playdateObjs = playdates.map(playdate => playdate.toObject());

    let startloc = {
      latitude: 0,
      longitude: 0
    };
    let endloc = {
      latitude: 0,
      longitude: 0
    };

    let distanceinMi = 0;

    User.findOne(req.userId, (err, user) => {
      if (err) {
        console.log(err);
        return res.status(400).json({error: 'High label error'});
      }
      if (user) {
        let playdateList = playdateObjs.map(playdate => {
          if (user.currentLocation && user.currentLocation.lat && user.currentLocation.lng) {
            let {lat, lng} = user.currentLocation;
            startloc['latitude'] = lat;
            startloc['longitude'] = lng;
          }
          if (playdate.location && playdate.location.lat && playdate.location.lng) {
            let {lat, lng} = playdate.location;
            endloc['latitude'] = lat;
            endloc['longitude'] = lng;
          }
          const distance = geolib.getDistance(startloc, endloc);
          distanceinMi = geolib.convertUnit("mi", distance, 1);

          if (playdate['location']) {
            playdate['location']['distance'] = `${distanceinMi} mi`;
          }
          return {record: playdate};
        });

        return res.status(200).json(playdateList);
      }
    });
  });
});

// Creating a playdate
router.post('/', [], async (req, res) => {
  logger.level = 'info';
  logger.info(`[POST] /playdates/ loginType: ${req.loginType}`);
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({errors: objToArr(errors.mapped())});
  }

  let {pets, place, startTime, endTime, status, nofityFriends, placeId, placeDetails} = req.body;

  let loc = await Location.findOne({placeId: placeId});
  if (loc) {
    logger.level = 'info';
    logger.info("Place is found in the database");
  } else {
    let locationRes = await createPlace(placeId);
    if (locationRes['success'] === true) {
      loc = locationRes['response']['data'];
    } else {
      return res.status(400).json({errors: "Create Playdate failed!"});
    }
  }

  try {
    let newPlaydate = {
      userId: req.userId,
      pets,
      place: loc.locName,
      startTime,
      endTime,
      status,
      nofityFriends: nofityFriends ? nofityFriends: false,
      location: loc._id,
      placeDetails: placeDetails ? placeDetails :  "Need to provide a place details"
    };

    Playdate.create(newPlaydate, async (err, playdate) => {
      if (err) {
        logger.level = 'error';
        logger.error(`playdate Creation Failed ${err}`);
        return res.status(400).json({errors: parseErrors(err.errors)});
      } else {
        //Find use push token service
        // const userTokens = await User.find({pushToken: {$ne: null} }).select('pushToken').sort({  updatedAt: -1});
        // let tokenArr = userTokens.map(userTk => userTk.pushToken );
        
        let lat = req.user.location.coordinates[1];
        let lng = req.user.location.coordinates[0];
        let locObj = { type: 'Point', coordinates: [lng, lat] };

        let users = await User.aggregate(
          [
            {
              "$geoNear": {
                "near": locObj,
                "distanceField": "distance",
                "spherical": true,
                "maxDistance": maxDistance,
                "distanceMultiplier": distanceMultiplier
              }
            },
            { $match: { pushToken : { $ne : null }} },
            { "$sort": { updatedAt: -1 } },
            {
              $project: {
                "distance" : { "$floor": "$distance" },
                "local.userName" : 1,
                "location": 1,
                "pushToken": 1
              }
            }
          ]
        )
        const tokenArr = users.map(user => user.pushToken );
        let pet = await Pet.findOne({ _id:  playdate.pets[0] });

        // Sending push notification
        let message = {
          "title": "New Playdate!",
          "body": `A new playdate has been created by ${pet.name}. Playdate will be at ${playdate.place}`
        }
        await pushNotification(tokenArr, message);

        // delayed notification fro review
        let job = queue.create('playDate-review', {
          id: playdate._id
        })
          .delay(moment(playdate.endTime).toDate())
          .save((err) => {
            if (!err) console.log(job.id);
          });

        // Feed Creation
        Feed.create({
          category: 1,
          record: playdate._id,
          onModel: 'Playdate'
        });

        Playdate.findOne({_id: playdate._id}).populate([
          {
            path: 'userId',
            model: 'User',
            select: 'local.email currentLocation profilePic'
          }, {
            path: 'pets',
            model: 'Pet',
            select: 'imagePath name'
          }, {
            path: 'location',
            model: 'Location'
          }
        ]).exec((err, playdate) => {
          return res.json({playdate});
        });
      }
    });
  } catch (e) {
    logger.error("Playdate creation failed" + e);
    return res.status(400).json({errors: 'Playdate creation failed!'});
  }
});

// Fetching playdate with id
router.get('/:id', (req, res) => {
  logger.info(`[GET] playdates/${req.params.id}`);

  Playdate.findOne({
    _id: req.params.id
  })
    .populate('pets location')
    .exec((err, playdate) => {
      if (err) {
        return res.status(400).json({errors: parseErrors(err.errors)});
      }
      if (!playdate) {
        return res.status(404).json({errors: "Playdate not found"});
      }

      let playdateObj = playdate.toObject();

      let startloc = {
        latitude: 32.78260925963876,
        longitude: -96.79933139703398
      };
      let endloc = {
        latitude: 0,
        longitude: 0
      };
      let distanceinMi = 0;

      User.findOne(req.userId, (err, user) => {
        if (err) {
          logger.error(`Error while find user ${e}`)
        }
        if (user) {
          if (user.currentLocation && user.currentLocation.lat && user.currentLocation.lng) {
            let {lat, lng} = user.currentLocation
            startloc['latitude'] = lat
            startloc['longitude'] = lng;
          }
          if (playdateObj.location && playdateObj.location.lat && playdateObj.location.lng) {
            let {lat, lng} = playdateObj.location;
            endloc['latitude'] = lat;
            endloc['longitude'] = lng;
          }

          const distance = geolib.getDistance(startloc, endloc);
          distanceinMi = geolib.convertUnit("mi", distance, 1);
        }

        playdateObj['location']['distance'] = `${distanceinMi} mi`;
        return res.status(200).json(playdateObj);
      });
    });
});

// Delete a playdate
router.delete('/:id', (req, res) => {
  Playdate.findOne({
    _id: req.params.id,
    userId: req.userId
  }, (err, playdate) => {
    if (err) {
      return res.status(400).json({errors: parseErrors(err.errors)});
    }

    if (playdate === null) {
      return res.status(404).json({errors: "Playdate not found"});
    }

    playdate.remove(function (err, playdate) {
      if (err) {
        return res.status(400).json({errors: parseErrors(err.errors)});
      }
      if (!playdate) {
        return res.status(404).json({errors: "Playdate not found"});
      }
      return res.status(200).json({message: "Playdate successfully deleted"});
    });
  });
});

//Update a playdate
router.put('/:id', [], async (req, res) => {
  console.log(req.body);

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({errors: objToArr(errors.mapped())});
  }

  let playdateId = req.params.id;
  let {pets, place, startTime, endTime, status, nofityFriends, placeId} = req.body;

  let loc = await Location.findOne({placeId: placeId});
  if (loc) {
    logger.level = 'info';
    logger.info("Place is found in the database");
  } else {
    let locationRes = await createPlace(placeId);
    if (locationRes['success'] === true) {
      console.log(locationRes);
      loc = locationRes['response']['data'];
    } else {
      return res.status(400).json({errors: "Create Playdate failed!"});
    }
  }

  let updatePlaydate = {
    userId: req.userId,
    pets,
    place: loc.locName,
    startTime,
    endTime,
    status,
    nofityFriends,
    location: loc._id
  };

  Playdate.findByIdAndUpdate(
    playdateId,
    updatePlaydate,
    {new: true}
    , (err, playdate) => {
      if (err) {
        return res.status(400).json({errors: parseErrors(err.errors)});
      }
      return res.status(200).json(playdate);
    });
});

//Join a playdate route
router.put('/join/:id', [], (req, res) => {
  logger.level = 'info';
  logger.info(`[PUT] /join/${req.params.id} route`);

  let joiningUserId = req.userId;
  let playdateId = req.params.id;
  let {pets} = req.body;

  Playdate.findByIdAndUpdate(
    playdateId,
    {new: true}
  )
    .populate({
      path: 'pets'
    })
    .exec((err, playdate) => {
      if (err) {
        return res.status(400).json({errors: parseErrors(err.errors)});
      }
      playdate.userJoining.push(joiningUserId);
      let newPetsArr = [...playdate.pets, ...pets];
      playdate.pets = newPetsArr;
      playdate.save((err, playdate) => {
        if (err) return res.status(400).json({errors: parseErrors(err.errors)});
        return res.status(200).json(playdate);
      })
    });
});

//Leave a playdate route
router.put('/leave/:id', [], (req, res) => {
  let joiningUserId = req.userId;
  let playdateId = req.params.id;

  Playdate.findByIdAndUpdate(
    playdateId,
    {new: true}
  )
    .populate({
      path: 'pets'
    })
    .exec((err, playdate) => {
      if (err) {
        return res.status(400).json({errors: parseErrors(err.errors)});
      }
      let pets = playdate.pets.filter(pd => {
        return String(pd.userId) !== String(joiningUserId)
      });
      playdate.userJoining.pull(joiningUserId);
      playdate.pets = pets;

      playdate.save((err, playdate) => {
        if (err) return res.status(400).json({errors: parseErrors(err.errors)});
        return res.status(200).json(playdate);
      });
    });
});

export default router;
