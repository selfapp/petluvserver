import express from 'express';
import log4js from 'log4js';
import axios from 'axios';

import Location from '../models/Location';
import googlePlaceDetails from '../Data/google_place_details.json';
import googlePlaceResultList from '../Data/google_place_search_results.json';
import authenticate from "../middlewares/authenticate";
import User from "../models/User";

import {distanceCalculator} from '../utils/distanceCalcutalor';
import { serviceTypeMapping } from '../utils/Constants';

const router = express.Router();
router.use(authenticate);

const logger = log4js.getLogger();
const GOOGLE_URL = "https://maps.googleapis.com/maps/api/place/textsearch/json";
const GOOGLE_PLACE_DETAILS_BASE_URL = "https://maps.googleapis.com/maps/api/place/details/json";
const GOOGLE_PHOTOS_BASE_URL = "https://maps.googleapis.com/maps/api/place/photo";

/*
    Return suggesstion list from google;
    /utility/placesuggestionsgoogle?searchKey="pet"
*/
router.get('/place-suggestions-google', async (req, res) => {
  logger.level = 'info';
  logger.info(`[GET] In route /place-suggestions-google. Query:${req.query.query}`);
  let data = null;
  let searchKey = "";
  let lat = "32.78260925963876";
  let lng = "-96.79933139703398";

  let latLong = `32.78260925963876,-96.79933139703398`;

  if (req.query.lat && req.query.lng) {
    lat = req.query.lat;
    lng = req.query.lng;
  }
  ;

  if (req.query.query) {
    searchKey = req.query.query;

    // Saving user current location
    let currentLocation = {lat, lng};
    let user = await User.findByIdAndUpdate(req.userId, {$set: {currentLocation: currentLocation}}, {new: true});
    if (!user) {
      // logger.level = 'error';
      console.error("Saving User location failed!. User Id: ", req.userId);
    }
    if (user.currentLocation.lat && user.currentLocation.lng) {
      latLong = `${user.currentLocation.lat},${user.currentLocation.lng}`;
    }

    const queryParams = `?key=${process.env.GOOGLE_API_KEY}&location=${latLong}&radius=10000&query=${searchKey}`;
    const url = `${GOOGLE_URL}${queryParams}`;

    try {
      let googleRes = await axios.get(url);
      let response = googleRes['data'];

      if (response.status === "OK") {
        logger.level = 'info';
        logger.info("Successfully retrived data from google. URL: " + url);

        data = response.results;
        data = data.map(function (location) {
          return {
            name: location.name ? location.name : "",
            place_id: location.place_id,
            formatted_address: location.formatted_address,
            location: location.geometry.location
          }
        });
        return res.status(200).json({
          success: true,
          msg: 'Successfully Retrived data from google.',
          response: {data}
        });
      } else if (response.status === "ZERO_RESULTS") {
        logger.level = 'info';
        logger.info("Google return zero results. URL: " + url);

        return res.status(200).json({
          success: true,
          msg: 'No results found.',
          response: {data: response.results}
        });
      } else {
        // logger.level = 'error';
        console.error("Invalid Request. URL: " + url);

        return res.status(400).json({
          success: false,
          msg: 'Invalid Request.',
          response: {data: data}
        });
      }
    } catch (e) {
      // logger.level = 'error';
      console.error("Error while getting while making request to google API." + e);

      return res.status(400).json({
        success: false,
        msg: 'Error while making request for gogole API.',
        response: {data: data}
      });
    }
  } else {
    // logger.level = 'error';
    console.error("Query params is Required!");

    return res.status(400).json({
      success: false,
      msg: 'SearchKey is missing in query params.',
      response: {data: data}
    });
  }
});

// Service suggesstion list - POST API
// Url construction: http://localhost:3001/api/places/services-suggestions-list?query="Pet Sitter"&lat=32.78260925963876&lng=-96.79933139703398
// body: { "comment": "Some comment", "speciesList": ["species1", "species2"] }
// Request body is optional for now. We need to work on that.
router.post('/services-suggestions-list', async (req, res) => {
  logger.level = 'info';
  logger.info(`[POST] /services-suggestions-list searchText = ${req.query.query} searchKey=${req.query.searchKey}. userId: ${req.userId}`);

  if (req.body.comment && req.body.speciesList) {
    let {comment, speciesList} = req.body;
  }

  let data = null;
  let searchText = "";
  let requestType = req.query.searchKey ? req.query.searchKey : "petSitter";
  let lat = req.user.currentLocation && req.user.currentLocation.lat ? req.user.currentLocation.lat : "32.78260925963876";
  let lng = req.user.currentLocation && req.user.currentLocation.lng ? req.user.currentLocation.lng : "-96.79933139703398";

  let mapping = {
    petSitter: 'petSitter',
    breeder: 'breeder',
    petGroomer: 'petGroomer',
    trainer: 'petTrainer',
    walker: 'dogWalker'
  };

  if (req.query.lat && req.query.lng) {
    lat = req.query.lat;
    lng = req.query.lng;
  }
  ;

  let latLong = `${lat},${lng}`;

  if (req.query.query) {
    searchText = req.query.query;

    // Saving user current location
    let currentLocation = {lat, lng};
    let user = await User.findByIdAndUpdate(req.userId, {$set: {currentLocation: currentLocation}}, {new: true});
    if (!user) {
      logger.level = 'error';
      logger.error("Saving User location failed!. User Id: ", req.userId);
    }

    if (user.currentLocation.lat && user.currentLocation.lng) {
      latLong = `${user.currentLocation.lat},${user.currentLocation.lng}`;
    }

    // Retrive in app service provider
    let serviceProviderList = await User.find({[`accountType.${mapping[requestType]}.isServiceProvider`]: true})
      .select(
        `local.userName businessUrl 
                currentLocation phoneNumber 
                accountType.${mapping[requestType]}.businessDescription 
                accountType.${mapping[requestType]}.businessImages`
      );
    if (!serviceProviderList) {
      logger.level = 'error';
      logger.error("Error retriving service provider list. Continue searching with google.");
    }
    if (serviceProviderList) {
      let startloc = {
        latitude: user.currentLocation.lat,
        longitude: user.currentLocation.lng
      };
      data = serviceProviderList.map(spl => {
        let splObj = spl.toObject();

        let endloc = {
          latitude: currentLocation.lat,
          longitude: currentLocation.lng
        }
        let distance = distanceCalculator(startloc, endloc, "mi");

        return {
          ...splObj,
          inAppServiceProvider: true,
          serviceProviderId: splObj._id,
          rating: 0,
          name: splObj.local.userName,
          serviceType: mapping[requestType].toUpperCase(),
          distance
        }
      })
    }

    console.log(data);

    // return res.json({ success: serviceProviderList });

    const queryParams = `?key=${process.env.GOOGLE_API_KEY}&location=${latLong}&radius=10000&query=${searchText}`;
    const url = `${GOOGLE_URL}${queryParams}`;

    try {
      // Need to turn on during production.<----------->
      // let googleRes = await axios.get(url);
      // let response = googleRes['data'];

      let response = googlePlaceResultList;

      if (response.status === "OK") {
        logger.level = 'info';
        logger.info("Successfully retrived data from google. URL: " + url);

        let startloc = {
          latitude: user.currentLocation.lat,
          longitude: user.currentLocation.lng
        };

        let resData = response.results;
        let googleDataMod = resData.map(function (location) {
          let endloc = {
            latitude: location.geometry.location.lat,
            longitude: location.geometry.location.lng
          }
          let distance = distanceCalculator(startloc, endloc, "mi");

          return {
            name: location.name ? location.name : "",
            place_id: location.place_id,
            formatted_address: location.formatted_address,
            location: location.geometry.location,
            isOpen: location.opening_hours ? location.opening_hours.open_now : false,
            rating: location.rating,
            serviceType: location.types[0],
            distance
          }
        });

        data = [...data, ...googleDataMod];

        return res.status(200).json({
          success: true,
          msg: 'Successfully Retrived data from google.',
          response: {data}
        });
      } else if (response.status === "ZERO_RESULTS") {
        logger.level = 'info';
        logger.info("Google return zero results. URL: " + url);

        return res.status(200).json({
          success: true,
          msg: 'No results found.',
          response: {data: response.results}
        });
      } else {
        logger.level = 'error';
        logger.error("Invalid Request. URL: " + url);

        return res.status(400).json({
          success: false,
          msg: 'Invalid Request.',
          response: {data: data}
        });
      }
    } catch (e) {
      logger.level = 'error';
      logger.error("Error while getting while making request to google API." + e);

      return res.status(200).json({
        success: false,
        msg: 'Error while making request for gogole API.Returing emtpy list. Might be service provider data exist.',
        response: {data: data}
      });
    }
  } else {
    logger.level = 'error';
    logger.error("Query params is Required!");

    return res.status(400).json({
      success: false,
      msg: 'SearchKey is missing in query params.',
      response: {data: data}
    });
  }


})


// API will return place details
router.get('/details/:placeId', async (req, res) => {
  logger.level = 'info';
  logger.info("Fetching place details with id" + req.params.id);

  let location = null;
  let placeId = req.params.placeId;

  location = await Location.findOne({placeId: placeId});
  if (location) {
    logger.level = 'info';
    logger.info("Location found in db" + placeId);

    return res.status(200).json({
      response: {
        success: true,
        data: location,
        msg: 'Place is found in db.'
      }
    });
  } else {
    let locationRes = await createPlace(placeId);
    if (locationRes['success'] === true) {
      location = locationRes['response']['data'];

      return res.status(200).json({
        response: {
          success: true,
          data: location,
          msg: 'Create location success.'
        }
      });
    } else {
      logger.level = 'error';
      logger.info("Crearte location failed! " + placeId);
      return res.status(400).json({
        response: {
          success: false,
          data: location,
          msg: 'Create location failed!.'
        }
      });
    }
  }
});


// Take a placeId as input
// return response obj with data
export const createPlace = async (placeId) => {
  let data = null;

  let placeObj = {
    placeId: "",
    category: "",
    photos: [],
    description: "",
    star: 0,
    hours: [],
    locName: "",
    provider: "google",
    phoneNumber: "",
    address: "",
    reviews: null,
    types: null
  };

  const queryParams = `?placeid=${placeId}&key=${process.env.GOOGLE_API_KEY}&fields=name,rating,formatted_phone_number,opening_hours,formatted_address,website,photo,review,place_id,type,geometry`;
  const url = `${GOOGLE_PLACE_DETAILS_BASE_URL}${queryParams}`;

  // retrive data from google details api.
  try {
    let googleRes = await axios.get(url);
    let response = googleRes['data'];

    // let response = googlePlaceDetails;

    if (response['status'] === 'OK') {
      logger.level = 'info';
      logger.info("Successfully retrived data from google. URL: " + url);

      let loc = null;
      try {
        let data = response["result"];
        placeObj['placeId'] = placeId;
        placeObj['phoneNumber'] = data['formatted_phone_number'] ? data['formatted_phone_number'] : "";
        placeObj['address'] = data['formatted_address'];
        placeObj['star'] = data['rating'] ? data['rating'] : 0;
        placeObj['website'] = data['website'] ? data['website'] : "";
        placeObj['locName'] = data['name'];
        placeObj['hours'] = data['opening_hours'] ? data['opening_hours']['weekday_text'] : null;
        placeObj['isOpen'] = data['opening_hours'] ? data['opening_hours']['open_now'] : false;
        placeObj['types'] = data['types'];
        placeObj['lat'] = data['geometry']['location']['lat'];
        placeObj['lng'] = data['geometry']['location']['lng'];

        const location = { type: 'Point', coordinates: [placeObj['lng'], placeObj['lat']] };
        placeObj['location'] = location;

        if (data['photos']) {
          let firstThreePhotos = data['photos'].slice(0, 3);
          let photos = firstThreePhotos.map(p => {
            let placeUrl = `${GOOGLE_PHOTOS_BASE_URL}?key=${process.env.GOOGLE_API_KEY}&photoreference=${p.photo_reference}&maxwidth=400`;
            return placeUrl;
          });
          placeObj['photos'] = [...placeObj['photos'], ...photos];
        }

        if (data['reviews']) {
          let firstThreeReviews = data['reviews'].slice(0, 3);
          placeObj['reviews'] = firstThreeReviews;
        }

        loc = await Location.create(placeObj);
        if (loc) {
          logger.level = 'info';
          logger.info("Successfully store place in db.");

          return {
            success: true,
            msg: 'Successfully fetched location details',
            response: {
              data: loc
            }
          }
        }
      } catch (e) {
        // logger.level = 'error';
        console.error("Error creating location.", error);
        return {
          success: false,
          msg: 'Error creating location obj',
          response: {data: null}
        };
      }

    } else {
      // logger.error = 'info';
      console.error("Could not find place details. URL: " + url);

      return {
        success: false,
        msg: 'Could not find the place details.',
        response: {data: data}
      };
    }
  } catch (e) {
    // logger.level = 'error';
    console.error("Error while making request to google place details API." + e);

    return {
      success: false,
      msg: 'Error while making request for gogole place details API.',
      response: {data: data}
    };
  }
};


/*
    Return suggesstion list from foursquare;
    /utility/placesuggestions?searchKey="pet"
*/
router.get('/placesuggestionsfoursquare', async (req, res) => {
  logger.level = 'trace';
  logger.trace("Getting places suggestions data. Route ->  /utility/placesuggestions");

  let data = null
  let searchKey = "";

  if (req.query.searchKey) {
    searchKey = req.query.searchKey;

    const latLong = `32.78260925963876,-96.79933139703398`;
    const queryParams = `?ll=${latLong}&limit=5&query=${searchKey}&v=20181210`;
    const authParams = `&client_id=${process.env.FOUR_SQUARE_CLIENT_ID}&client_secret=${process.env.FOUR_SQUARE_CLIENT_SECRET}`;
    const url = `https://api.foursquare.com/v2/venues/suggestcompletion${queryParams}${authParams}`;


    try {
      let placesuggestions = await axios.get(url);

      if (placesuggestions.data.meta.code === 200) {
        logger.level = 'info';
        logger.info("Successfully retrived data from forsquare. URL: " + url);

        data = placesuggestions.data.response.minivenues;
        return res.status(200).json({
          success: true,
          msg: 'Successfully Retrived data from forsquare.',
          response: {data}
        });
      } else {
        // logger.level = 'error';
        console.error("Could not fetch data from forsquare API. URL: " + url);

        return res.status(400).json({
          success: false,
          msg: 'Data fetch failed from forsquare returnign empty array.',
          response: {data: []}
        });
      }
    } catch (e) {
      // logger.level = 'error';
      console.error("Error while getting while making request to forsquare API." + e);

      return res.status(400).json({
        success: false,
        msg: 'Error while making request for forquare API.',
        response: {data: []}
      })
    }
  } else {
    // logger.level = 'error';
    console.error("Query params is Required!");

    return res.status(400).json({
      success: false,
      msg: 'SearchKey is missing in query params.',
      response: {data: []}
    })
  }

});

export default router;


// To render photos
// https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyA2mlROpVOxhnKCjHDd2L7_Y-_IzQJ_zJk&photoreference=CmRaAAAA3MbVv_KQt3LG8pdadvNnn5MdH_omziVaJ5D0H3Y1untq1wzsmxEYKZAzIWHiebzB0cUHMsPgVUIqBbESSmt4fJmCFZD-8QD2RgWsSXqBni4Hu46vrIM3vb5Ea3DNPZWLEhCqOAC1mfGizKZRZpA2R87pGhRQdkuAQ6A_0xBGmLNnOgaADYnr7Q&maxwidth=400
