import AWS from 'aws-sdk';

let uploadToS3 = async (file) => {

    try {
        AWS.config.setPromisesDependency(null);
        AWS.config.update({
            accessKeyId: process.env.AWS_ACCESS_KEY,
            secretAccessKey: process.env.AWS_SECRET_KEY,
        });

        let s3bucket = new AWS.S3({
            Bucket: process.env.BUCKET_NAME
        });

        let url = `https://s3.amazonaws.com/${process.env.BUCKET_NAME}/${file.name}`;

        let params = { Bucket: 'petluvs', Key: file.name, Body: file.data };

        let response = await s3bucket.putObject(params).promise();

        if (response.ETag) {
            // return `${imageLocation}`;
            return { imageName: file.name,  url };
        }
        return null;

    } catch (e) {
        // console.log('error', e);
        return null;
    }
};

export { uploadToS3 }
