import AWS from 'aws-sdk';
import log4js from 'log4js';
const logger = log4js.getLogger();

let uploadToS3V2 = async (file, pathName, id) => {
    logger.info(`[INFO] in uploadToS3V2() function`);
    try {
        AWS.config.setPromisesDependency(null);
        AWS.config.update({
            accessKeyId: process.env.AWS_ACCESS_KEY,
            secretAccessKey: process.env.AWS_SECRET_KEY,
        });

        let s3bucket = new AWS.S3({ Bucket: process.env.BUCKET_NAME});

        let url = `https://s3.amazonaws.com/petluvsstore/${pathName}/${id}/${file.name}`;

        let params = { Bucket: `petluvsstore/${pathName}/${id}`, Key: file.name, Body: file.data, ACL: 'public-read', };

        let response = await s3bucket.putObject(params).promise();
        if (response.ETag) {
            logger.info(`Successfully uploaded media. ETag = ${response.ETag}`);
            return { imageName: file.name,  url };
        }
        logger.info("could not uploaded media.");
        return null;
    } catch (e) {
        logger.error(`Error while uploading media ${e}`);
        return null;
    }
};

export { uploadToS3V2 }
