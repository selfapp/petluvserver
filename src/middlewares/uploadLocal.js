import express from 'express';
import fs from 'fs';

export default (req, res, next) => {
    if(req.files !== null && req.files !== undefined) {
        let imageFile = req.files.file;

        let dir = __dirname + `/../upload/${req.userId}`;
        console.log(ensureExists(dir));

        if (imageFile) {
            imageFile.mv(`${dir}/${imageFile.name}`, function(err) {
			    if (err) {
			      return res.status(500).send(err);
			    }
			    req.body.imagePath = `${dir}/${imageFile.name}`;
			    next();
			});
        }
    }else{
        console.log('No file was sent');
        next();
    }
};

let ensureExists = (path) => {
    console.log(path);
    fs.mkdir(path, err => {
        if(err) {
            if (err.code == 'EEXIST') return true; // ignore the error if the folder already exists
            else return false;  // something else went wrong
        } else true;  //Successfull created folder
    });
};

// function to encode file data to base64 encoded string
function base64_encode(file) {
    // // read binary data
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap).toString('base64');
}