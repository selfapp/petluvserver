import jwt from 'jsonwebtoken';
import log4js from 'log4js';

import User from '../models/User';
const logger = log4js.getLogger();

export default (req, res, next) => {
    const header = req.headers.authorization;
    let token;

    if (header) token = header.split(' ')[1];

    if (token) {
        jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
            if (err) {
                return res.status(401).json({errors: {global: 'Invalid token'}});
            } else {
                if (decoded.id) {
                    User.findOne({_id: decoded.id})
                        .populate({ 
                            path: "role"
                        })
                    .then(user => {
                        if (user) {
                            req.userId = user._id;
                            req.user = user;
                            req.loginType = decoded.loginType
                            
                            // enable this when deploying to production
                            // if (decoded.loginType === 'local') {
                            //     if (!user.local.confirm) {
                            //         return res.status(400).json({errors: {global: 'You have not confirmed your email.'}});
                            //     }
                            // }
                            next();
                        } else {
                            return res.status(401).json({errors: {global: 'This user has been delete from database.'}});
                        }
                    });
                } else {
                    return res.status(401).json({errors: {global: 'Invalid token'}});
                }
            }
        });
    } else {
        return res.status(401).json({errors: {global: 'no token'}});
    }
};
