import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import cors from 'cors';
import validator from 'express-validator';
import fileUpload from 'express-fileupload';
import passport from 'passport';
import swaggerUi from 'swagger-ui-express';
import YAML from 'yamljs';
import './polyfill';
dotenv.config();

import auth from './routes/auth';
import home from './routes/home';
import users from './routes/users';
import pets from './routes/pets';
import breadingevents from './routes/breadingevents';
import playdates from './routes/playdates';
import testroutes from './routes/testroutes';
import services from './routes/services';
import reviews from './routes/reviews';
import notifications from './routes/notifications';
import petBreeds from './routes/petBreeds';
import places from './routes/places';
import media from './routes/media';

import testNotifications from './testroutes/notifications';


import processJobs from './services/jobs';
// import swaggerDocument from './Data/swagger.json';
const swaggerDocument = YAML.load(__dirname +'/Data/swagger.yaml');

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ limit: '200mb' , extended: false }));
app.use(bodyParser({ limit: '200mb' }))
app.use(fileUpload());
app.use(validator());
app.set('view engine', 'ejs');
app.set('views', __dirname + '/public/views');

let social 	= require('./config/social-config')(app, passport);
let instragramLogin = require('./config/instragram-login-config')(app, passport);

// app.use('/upload', authenticate, express.static(__dirname + '/upload'));

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

require('./config/kuePortConfig');
require('./config/mongoose');

app.use('/api/auth', auth);
app.use('/api/home', home);
app.use('/api/users', users);
app.use('/api/pets', pets);
app.use('/api/breadingevents', breadingevents);
app.use('/api/playdates', playdates);
app.use('/api/services', services);
app.use('/api/testroutes', testroutes);
app.use('/api/reviews', reviews);
app.use('/api/notifications', notifications);
app.use('/api/pet-breeds', petBreeds);
app.use('/api/places', places);
app.use('/api/media', media);

app.use('/api/test/notifications', testNotifications);

app.get('/*', (req, res) => {
	res.sendFile(path.join(__dirname, './public/index.html'));
});

app.listen(process.env.SERVER_PORT || 3000, '0.0.0.0',
	() => console.log(`Running on ${process.env.HOST}:${process.env.SERVER_PORT}`));

processJobs();

// To run seeds uncomment this line and restart the server once. Comment after use.
// require('./config/seeds');
