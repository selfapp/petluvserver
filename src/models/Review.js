import mongoose from 'mongoose';

const ReviewSchema = new mongoose.Schema({
  starCount: {
    type: Number,
    required: true,
    min: 0,
    max: 5,
    default: 0
  },
  description: {
    type: String,
    maxlength: [500, "Description should be less then 200 characters long"]
  },
  dataId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    refPath: 'reviewFor'
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  reviewFor: {
    type: String,
    required: true,
    enum: ['Playdate']
  },
  reviewerId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  receiverId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
}, {timestamps: true});

export default mongoose.model('Review', ReviewSchema);
