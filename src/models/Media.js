import mongoose from 'mongoose';
import {Schema, model} from 'mongoose';

const Media = new Schema({
    url: String, 
    mediaType: String,
    mediaName: String,
    petId: {
        type: mongoose.Schema.Types.ObjectId,
		ref: 'Pet'
    }
}, { timestamps: true });

export default model('Media', Media);
