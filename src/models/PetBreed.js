import mongoose from 'mongoose';

const PetBreedSchema = mongoose.Schema(
    {
        speciesName: {
            type: String,
            required: true
        },
        breeds: {
            type: Array,
            required: true
        }
    },
    {
        timestamps: true
    }
);

export default mongoose.model('PetBreed', PetBreedSchema);
