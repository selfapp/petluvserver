import mongoose from 'mongoose';
import mongoosePaginate from "mongoose-paginate";

/*
* Category Values:
* 1 - Playdate
* 2 - Review
* 3 - BreaderEvent
* 4 - Media
* */

const FeedSchema = mongoose.Schema(
  {
    category: {
      type: Number,
      required: true
    },
    record: {
      type: mongoose.Types.ObjectId,
      required: true,
      refPath: 'onModel'
    },
    onModel: {
      type: String,
      required: true,
      enum: ['Playdate', 'Review', 'BreaderEvent', 'Media']
    }
  },
  {
    timestamps: true
  }
);

FeedSchema.plugin(mongoosePaginate);

export default mongoose.model('Feed', FeedSchema);
