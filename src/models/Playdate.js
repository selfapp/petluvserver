import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

// pet list are going to be array
// by default status should be public

const PlaydateSchema = new mongoose.Schema({
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	},
	pets: [
		{ 
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Pet'
		}
	],
	place: { 
		type: String,
		required: [true, "field cannot be blank"],
		maxlength: [100, "should be less then 100"]
	},
	placeDetails: {
		type: String,
		required: [true, "field cannot be blank"],
		maxlength: [300, "should be less then 300"]
	},
	startTime: {
		type: Date,
		required: [true, "Field cannot be empty"]
	},
	endTime: {
		type: Date,
		required: [true, "Field cannot be empty"]
	},
	status: {
        type: String,
        enum: ['public', 'private'],
		required: true
	},
	nofityFriends: {
		type: Boolean,
		default: false
	},
	userJoining: [
		{ 
			type: mongoose.Schema.Types.ObjectId,
			ref: 'User'
		}
	],
	reviews: [
		{ 
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Review'
		}
	],
	location: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Location'
	}
}, { timestamps: true });

PlaydateSchema.plugin(mongoosePaginate);

export default mongoose.model('Playdate', PlaydateSchema);