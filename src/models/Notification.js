import mongoose from 'mongoose';

const NotificationSchema = mongoose.Schema(
  {
    body: {
      type: String,
      required: true
    },
    category: {
      type: Number,
      required: true
    },
    read: {
      type: Boolean,
      required: true,
      default: false
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    dataId: {
      type: mongoose.Schema.Types.ObjectId
    },
    image: {
      type: String
    }
  },
  {
    timestamps: true
  }
);

export default mongoose.model('Notification', NotificationSchema);
