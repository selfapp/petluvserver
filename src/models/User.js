import mongoose from "mongoose";
import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";
import uniqueValidator from "mongoose-unique-validator";
import mongoosePaginate from "mongoose-paginate";

import PointSchema from './PointSchema';

import { petluvsDefaultImage } from '../utils/Constants';

const UserSchema = new mongoose.Schema(
  {
    local: {
      email: {
        type: String,
        validate: {
          validator: function (v) {
            return /\S*\@\S*\.\S+/g.test(v);
          },
          message: "You must provide a valid email"
        }
      },
      passwordHash: {
        type: String
      },
      confirmed: {type: Boolean, default: false},
      confirmationToken: {type: String, default: ""},
      forgotPasswordToken: {type: String, default: null},
      userName: {type: String, default: "PetLuvs User"},
      photo: {
        type: String,
        default: petluvsDefaultImage
      }
    },
    social: {
      fb: {
        id: String,
        token: String,
        email: String,
        photo: String,
        userName: String
      },
      gmail: {
        id: String,
        token: String,
        email: String,
        photo: String,
        userName: String
      }
    },
    pushToken: {type: String, default: null},
    currentLocation: {
      lat: {type: Number, default: 32.78260925963876},
      lng: {type: Number, default: -96.79933139703398}
    },
    location: {
      type: PointSchema,
      required: false
    },
    enableServices: {
      type: Array,
      default: ["SITTER", "GROOMER", "TRAINER", "FOSTER_PETS", "RESCUE_ANIMALS"]
    },
    accountType: {
      breeder: {
        isServiceProvider: {type: Boolean, default: false},
        species: {type: Array},
        breeds: {type: Array},
        akcNumber: {type: String},
        akcImage: {type: String},
        businessDescription: {type: String},
        businessImages: {type: String}
      },
      daycareAndBoarders: {
        isServiceProvider: {type: Boolean, default: false},
        species: {type: Array},
        businessDescription: {type: String},
        businessImages: {type: String}
      },
      petGroomer: {
        isServiceProvider: {type: Boolean, default: false},
        species: {type: Array},
        businessDescription: {type: String},
        businessImages: {type: String}
      },
      petTrainer: {
        isServiceProvider: {type: Boolean, default: false},
        species: {type: Array},
        businessDescription: {type: String},
        businessImages: {type: String}
      },
      fosterPets: {
        isServiceProvider: {type: Boolean, default: false},
        businessDescription: {type: String},
        imagePath: {type: String},
        businessImages: {type: String}
      },
      rescueAnimals: {
        isServiceProvider: {type: Boolean, default: false},
        businessDescription: {type: String},
        imagePath: {type: String},
        businessImages: {type: String}
      },
      dogWalker: {
        isServiceProvider: {type: Boolean, default: false},
        breeds: {type: Array},
        businessDescription: {type: String},
        businessImages: {type: String}
      },
      petSitter: {
        isServiceProvider: {type: Boolean, default: false},
        species: {type: Array},
        businessDescription: {type: String},
        businessImages: {type: String}
      }
    },
    businessEmail: {
      type: String,
      validate: {
        validator: function (v) {
          return /\S*\@\S*\.\S+/g.test(v);
        },
        message: "You must provide a valid email"
      }
    },
    address: {type: String},
    phoneNumber: {type: String},
    breederInfo: {
      akaCertNumber: {type: String},
      certificateImage: {type: String}
    },
    businessUrl: {type: String, default: ""},
    profilePic: {type: String, default: petluvsDefaultImage},
    idImage: {type: String},
    ssn: {type: String},
    role: {type: mongoose.Schema.Types.ObjectId, ref: "Role"},
    pets: [{type: mongoose.Schema.Types.ObjectId, ref: "Pet"}],
    playdates: [{type: mongoose.Schema.Types.ObjectId, ref: "Playdate"}],
    reviews: [{ type: mongoose.Schema.Types.ObjectId, ref: "Review"}]
  },
  {timestamps: true}
);

UserSchema.methods.isValidPassword = function isValidPassword(password) {
  return bcrypt.compareSync(password, this.local.passwordHash);
};

UserSchema.methods.isPreviousPassword = function isValidPassword(password) {
  return bcrypt.compareSync(password, this.local.passwordHash);
};

UserSchema.methods.generateJWt = function generateJWt(loginType) {
  return jwt.sign(
    {
      id: this._id,
      confirmed: this.local.confirmed,
      enableServices: this.enableServices,
      loginType
    },
    process.env.JWT_SECRET
  );
};

UserSchema.methods.generateConfirmationEmilaJWt = function generateConfirmationEmilaJWt() {
  return jwt.sign(
    {
      id: this._id,
      confirmed: this.local.confirmed
    },
    process.env.JWT_SECRET,
    {expiresIn: process.env.EMIL_TOKEN_EXPIRE_TIME}
  );
};

UserSchema.methods.setPassword = function setPassword(password) {
  this.local.passwordHash = bcrypt.hashSync(password, bcrypt.genSaltSync(10));
};

UserSchema.methods.setConfirmationToken = function setConfirmationToken() {
  this.local.confirmationToken = this.generateConfirmationEmilaJWt();
};

// Forgot password save
UserSchema.methods.setForgotPasswodToken = function setForgotPasswodToken(
  password
) {
  this.local.forgotPasswordToken = password;
};

UserSchema.methods.toAuthJSON = function toAuthJSON(loginType) {
  return { token: this.generateJWt(loginType)};
};

UserSchema.methods.generateConfirmationUrl = function generateConfirmationUrl() {
  let HOST = `${process.env.HOST}:${process.env.SERVER_PORT}`;
  if (process.env.PETLUVS_ENV === 'PROD') {
    HOST = `${process.env.HOST}`;
  }
  return `${HOST}/api/auth/verify?confirmationToken=${this.local.confirmationToken}`;
};

UserSchema.index({location:"2dsphere"});
UserSchema.plugin(uniqueValidator, {message: "This email already taken"});
UserSchema.plugin(mongoosePaginate);

export default mongoose.model("User", UserSchema);
