import mongoose from 'mongoose';
import PointSchema from './PointSchema';

const LocationSchema = new mongoose.Schema({
	placeId: { type: String },
    locName: { type: String },
    lat: { type: Number },
    lng: { type: Number },
    description: { type: String },
    serviceType: { type: String },
    star: { type: Number },
    isOpen: { type: Boolean },
    hours: [{ type: String }],
    phoneNumber: { type: String },
    address: { type: String },
    website: { type: String },
    provider: { type: String },
    photos: [ { type: String }],
    reviews: [
        {
            author_name: String,
            author_url: String,
            language: String,
            profile_photo_url: String,
            rating: Number,
            relative_time_description: String,
            text: String,
            time: String
        }
    ],
    location: {
        type: PointSchema,
        required: true
    },
    types:[ { type: String }]
}, { timestamps: true });

LocationSchema.index({location:"2dsphere"});

export default mongoose.model('Location', LocationSchema);