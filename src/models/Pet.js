import mongoose from "mongoose";
import {Schema, model} from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const PetSchema = new Schema({
	userId: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	name: {
		type: String,
		required: [true, "Name cannot be blank"],
		maxlength: [100, "Name should be less then 100 characters long"]
	},
	species: {
		type: String,
		required: [true, "Species cannot be blank"],
		maxlength: [100, "Species should be less then 100"]
	},
	breed: {
		type: String,
		required: [true, "Field cannot be blank"],
		maxlength: [100, "Length should be less then 100"]
	},
	birthday: {
		type: Date,
		required: [true, "Field cannot be empty"]
	},
	attributes:{
		_id: false,
		fullyVaccinated: Boolean,
		insured: Boolean,
		fixed: Boolean
	},
	imagePath: [{
		_id: false,
		imageName: String,
		url: String
	}],
	media: [{type: mongoose.Schema.Types.ObjectId, ref: "Media"}]

	// media: [{ 
	// 	_id: false, 
	// 	url: String, 
	// 	mediaType: String,
	// 	mediaName: String
	// }]
}, { timestamps: true });

PetSchema.plugin(mongoosePaginate);

export default model('Pet', PetSchema);
