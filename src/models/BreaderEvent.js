import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';

const BreaderEventSchema = new mongoose.Schema({
	breederInfo: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	},
	pets: [
		{ 
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Pet'
		}
	],
	place: { 
		type: String,
		required: [true, "field cannot be blank"],
		maxlength: [100, "should be less then 100"]
	},
	startTime: {
		type: Date,
		required: [true, "Field cannot be empty"]
	},
	endTime: {
		type: Date,
		required: [true, "Field cannot be empty"]
	},
	status: {
        type: String,
        enum: ['public', 'private'],
		required: true
	}
}, { timestamps: true });


BreaderEventSchema.plugin(mongoosePaginate);

export default mongoose.model('BreaderEvent', BreaderEventSchema);