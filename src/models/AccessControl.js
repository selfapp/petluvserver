import mongoose from 'mongoose';

const AccessControlSchema = new mongoose.Schema({
    accessType: {
        type: Array,
        enum : ['ALL','READ', 'WRITE', 'DELETE'],
        default: ['ALL']
    }
}, {timestamps: true});

export default mongoose.model('AccessControl', AccessControlSchema);
