import mongoose from 'mongoose';

const ServiceSchema = new mongoose.Schema({
    breed: {
        type: String
    },
    durationType: {
        type: String
    },
    duration: {
        type: Number
    },
    service: {
        type: String
    },
    comments: {
        type: String
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
}, {timestamps: true});


export default mongoose.model('Service', ServiceSchema);
