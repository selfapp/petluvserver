import mongoose from 'mongoose';

const RoleSchema = new mongoose.Schema({
    roleType: {
        type: String,
        enum : ['USER','ADMIN'],
        default: 'USER'
    },
}, {timestamps: true});

export default mongoose.model('Role', RoleSchema);
