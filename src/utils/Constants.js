export const serviceTypeMapping = {
    petSitter: 'petSitter',
    breeder: 'breeder',
    groomer: 'petGroomer',
    trainer: 'petTrainer',
    walker: 'dogWalker'
};

export const petluvsDefaultImage = 'https://petluvsstore.s3.amazonaws.com/Petluvs-defaults/petluvs.png';