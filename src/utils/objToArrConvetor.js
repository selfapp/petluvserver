
//Error Object model 1
// export default function(errors) {
//     let errorObjArr = [];

// 	Object.keys(errors).forEach(i => {
// 			let newObj = {
// 				[i]: errors[i]
// 			};
// 			errorObjArr.push(newObj);
//     });
    
//     return errorObjArr;
// };

//Error object model 2
export default function(errors) {
    let errorObjArr = [];
	Object.keys(errors).forEach(i => {
		let newObj = {
			[i]: errors[i].msg
		};
		errorObjArr.push(newObj);
	});
    
    return errorObjArr;
};