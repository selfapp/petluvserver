import geolib from 'geolib';
import log4js from 'log4js';

const logger = log4js.getLogger();

export const distanceCalculator = (startLoc, endLoc, unit) => {
    if (startLoc && endLoc) {
        const distance = geolib.getDistance(startLoc, endLoc);
        let distanceInUnit = geolib.convertUnit(unit, distance, 1);
        return `${distanceInUnit} ${unit}`;
    }else {
        logger.level = 'error';
        logger.error('Could not calculate distance. Data is not valid. Returning 0 mi. Params: ', startLoc, endLoc, unit);
        return 0 + " mi";
    }
};