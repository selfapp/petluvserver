import mongoose from 'mongoose';

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_DEV_URL)
	.then(() => console.log('Connected to database petluvs'))
	.catch(err => {
		console.log('Could Not connected to database');
	});
