import InstagramTokenStrategy from 'passport-instagram-token';

module.exports = function(app, passport) {
	app.use(passport.initialize());
	app.use(passport.session());

	passport.serializeUser(function(user, done) {
  		done(null, user._id);
	});

	passport.deserializeUser(function(user, done) {
  		done(null, user);
    });
    
    passport.use(new InstagramTokenStrategy({
        clientID: process.env.INSTAGRAM_CLIENT_ID,
        clientSecret: process.env.INSTAGRAM_CLIENT_SECRET,
        passReqToCallback: true
    }, function(req, accessToken, refreshToken, profile, next) {
        console.log(accessToken, refreshToken, profile);
    }));
};