import kue from 'kue';
import dotenv from 'dotenv';
dotenv.config();

export let queConn = kue.createQueue({
    redis: {
        port: '6379',
        host: process.env.REDIS_HOST
    }
});
