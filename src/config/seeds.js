require('./mongoose');

import PetBreed from '../models/PetBreed';

const data = [
    {
        speciesName: 'Alpacas',
        breeds: [
            'Suri',
            'Huacaya'
        ]
    },
    {
        speciesName: 'Ant Farm',
        breeds: ['NA']
    },
    {
        speciesName: 'Bird',
        breeds: [
            'Cockatiels',
            'Budgerigars/Parakeets',
            'Cockatoos',
            'Conures',
            'Macaws',
            'Poicephalus',
            'Amazon Parrots',
            'Quaker Parrots',
            'Pionus Parrots',
            'African Grey Parrot',
            'Quaker Parrot',
            'Caiques',
            'Finches',
            'Canaries'
        ]
    },
    {
        speciesName: 'Cat',
        breeds: [
            'Maine Coon',
            'American Shorthair',
            'Persian',
            'Ragdoll',
            'Siamese',
            'Abyssinian',
            'Himalayan',
            'Exotic Shorthair',
            'Burmese',
            'Bengal',
            'Sphynx',
            'Russian Blue',
            'Birman',
            'British Shorthair',
            'Scottish Fold',
            'Manx',
            'Burmese'
        ]
    },
    {
        speciesName: 'Chinchilla',
        breeds: [
            'Long-tailed',
            'Short-Tailed'
        ]
    },
    {
        speciesName: 'Dog',
        breeds: [
            'Affenpinscher',
            'Afghan Hound',
            'Airedale Terrier',
            'Akita',
            'Alaskan Klee Kai',
            'Alaskan Malamute',
            'American Bulldog',
            'American English Coonhound',
            'American Eskimo Dog',
            'American Foxhound',
            'American Pit Bull Terrier',
            'American Staffordshire Terrier',
            'American Water Spaniel',
            'Anatolian Shepherd Dog',
            'Appenzeller Sennenhunde',
            'Australian Cattle Dog',
            'Australian Kelpie',
            'Australian Shepherd',
            'Australian Terrier',
            'Azawakh',
            'Barbet',
            'Basenji',
            'Basset Hound',
            'Beagle',
            'Bearded Collie',
            'Bedlington Terrier',
            'Belgian Malinois',
            'Belgian Sheepdog',
            'Belgian Tervuren',
            'Berger Picard',
            'Bernedoodle',
            'Bernese Mountain Dog',
            'Bichon Frise',
            'Black and Tan Coonhound',
            'Black Mouth Cur',
            'Black Russian Terrier',
            'Bloodhound',
            'Blue Lacy',
            'Bluetick Coonhound',
            'Boerboel',
            'Bolognese',
            'Border Collie',
            'Border Terrier',
            'Borzoi',
            'Boston Terrier',
            'Bouvier des Flandres',
            'Boxer',
            'Boykin Spaniel',
            'Bracco Italiano',
            'Briard',
            'Brittany',
            'Brussels Griffon',
            'Bull Terrier',
            'Bulldog',
            'Bullmastiff',
            'Cairn Terrier',
            'Canaan Dog',
            'Cane Corso',
            'Cardigan Welsh Corgi',
            'Catahoula Leopard Dog',
            'Caucasian Shepherd Dog',
            'Cavalier King Charles Spaniel',
            'Cesky Terrier',
            'Chesapeake Bay Retriever',
            'Chihuahua',
            'Chinese Crested',
            'Chinese Shar-Pei',
            'Chinook',
            'Chow Chow',
            'Clumber Spaniel',
            'Cockapoo',
            'Cocker Spaniel',
            'Collie',
            'Coton de Tulear',
            'Curly-Coated Retriever',
            'Dachshund',
            'Dalmatian',
            'Dandie Dinmont Terrier',
            'Doberman Pinscher',
            'Dogo Argentino',
            'Dogue de Bordeaux',
            'Dutch Shepherd',
            'English Cocker Spaniel',
            'English Foxhound',
            'English Setter',
            'English Springer Spaniel',
            'English Toy Spaniel',
            'Entlebucher Mountain Dog',
            'Field Spaniel',
            'Finnish Lapphund',
            'Finnish Spitz',
            'Flat-Coated Retriever',
            'Fox Terrier',
            'French Bulldog',
            'German Pinscher',
            'German Shepherd Dog',
            'German Shorthaired Pointer',
            'German Wirehaired Pointer',
            'Giant Schnauzer',
            'Glen of Imaal Terrier',
            'Goldador',
            'Golden Retriever',
            'Goldendoodle',
            'Gordon Setter',
            'Great Dane',
            'Great Pyrenees',
            'Greater Swiss Mountain Dog',
            'Greyhound',
            'Harrier',
            'Havanese',
            'Ibizan Hound',
            'Icelandic Sheepdog',
            'Irish Red and White Setter',
            'Irish Setter',
            'Irish Terrier',
            'Irish Water Spaniel',
            'Irish Wolfhound',
            'Italian Greyhound',
            'Jack Russell Terrier',
            'Japanese Chin',
            'Japanese Spitz',
            'Karelian Bear Dog',
            'Keeshond',
            'Kerry Blue Terrier',
            'Komondor',
            'Kooikerhondje',
            'Korean Jindo Dog',
            'Kuvasz',
            'Labradoodle',
            'Labrador Retriever',
            'Lagotto Romagnolo',
            'Lakeland Terrier',
            'Lancashire Heeler',
            'Leonberger',
            'Lhasa Apso',
            'Lowchen',
            'Maltese',
            'Maltese Shih Tzu',
            'Maltipoo',
            'Manchester Terrier',
            'Mastiff',
            'Miniature Pinscher',
            'Miniature Schnauzer',
            'Mudi',
            'Mutt',
            'Neapolitan Mastiff',
            'Newfoundland',
            'Norfolk Terrier',
            'Norwegian Buhund',
            'Norwegian Elkhound',
            'Norwegian Lundehund',
            'Norwich Terrier',
            'Nova Scotia Duck Tolling Retriever',
            'Old English Sheepdog',
            'Otterhound',
            'Papillon',
            'Peekapoo',
            'Pekingese',
            'Pembroke Welsh Corgi',
            'Petit Basset Griffon Vendeen',
            'Pharaoh Hound',
            'Plott',
            'Pocket Beagle',
            'Pointer',
            'Polish Lowland Sheepdog',
            'Pomeranian',
            'Pomsky',
            'Poodle',
            'Portuguese Water Dog',
            'Pug',
            'Puggle',
            'Puli',
            'Pyrenean Shepherd',
            'Rat Terrier',
            'Redbone Coonhound',
            'Rhodesian Ridgeback',
            'Rottweiler',
            'Saint Bernard',
            'Saluki',
            'Samoyed',
            'Schipperke',
            'Schnoodle',
            'Scottish Deerhound',
            'Scottish Terrier',
            'Sealyham Terrier',
            'Shetland Sheepdog',
            'Shiba Inu',
            'Shih Tzu',
            'Siberian Husky',
            'Silken Windhound',
            'Silky Terrier',
            'Skye Terrier',
            'Sloughi',
            'Small Munsterlander Pointer',
            'Soft Coated Wheaten Terrier',
            'Stabyhoun',
            'Staffordshire Bull Terrier',
            'Standard Schnauzer',
            'Sussex Spaniel',
            'Swedish Vallhund',
            'Tibetan Mastiff',
            'Tibetan Spaniel',
            'Tibetan Terrier',
            'Toy Fox Terrier',
            'Treeing Tennessee Brindle',
            'Treeing Walker Coonhound',
            'Vizsla',
            'Weimaraner',
            'Welsh Springer Spaniel',
            'Welsh Terrier',
            'West Highland White Terrier',
            'Whippet',
            'Wirehaired Pointing Griffon',
            'Xoloitzcuintli',
            'Yorkipoo',
            'Yorkshire Terrier'
        ]
    },
    {
        speciesName: 'Ferret',
        breeds: [
            'Albino',
            'Black Sable',
            'Black Sable Mitt',
            'Blaze',
            'Champagne',
            'Chocolate',
            'Chocolate Mitt',
            'Cinnamon',
            'Cinnamon Mitts',
            'Dalmatian',
            'Heavy Silver or Pewter',
            'Panda',
            'Light Pattern',
            'Medium Pattern',
            'Sable',
            'Sable Mitt',
            'Siamese',
            'Siamese Mitt',
            'Striped White',
            'White with Dark Eyes'
        ]
    },
    {
        speciesName: 'Fish',
        breeds: [
            'Neon Tetra',
            'Danios',
            'Platies',
            'Guppies',
            'Kuhli Loach',
            'Cherry Barb',
            'Fire Mouth Cichilid',
            'Pearl Gourami',
            'Tiger Pleco',
            'Cory Catfish',
            'Mollies',
            'Sword Tails',
            'Betta'
        ]
    },
    {
        speciesName: 'Frog/Toad',
        breeds: [
            'Pacman Frog',
            'Tomato Frog',
            'Fire Belly Toad',
            'White\'s Tree Frog',
            'Waxy Monkey Frog',
            'Red Eye Tree Frog',
            'Dwarf Underwater Frog'
        ]
    },
    {
        speciesName: 'Gecko',
        breeds: [
            'Leopard Gecko',
            'Red Ackie',
            'Bearded Dragon',
            'Crested Gecko',
            'Argentine Black and White Tegu',
            'Iguana',
            'Savannah Monitor',
            'Green Anole',
            'Chameleon'
        ]
    },
    {
        speciesName: 'Gerbil',
        breeds: [
            'Lybian gerbil',
            'Egyptian gerbil',
            'Indian gerbil',
            'Jerusalem gerbil',
            'Namib Paeba gerbil',
            'Przewalski\'s gerbil'
        ]
    },
    {
        speciesName: 'Goat',
        breeds: [
            'Nubian',
            'La Mancha',
            'Alpine',
            'Oberhasli',
            'Toggenburg',
            'Saanen',
            'Sable',
            'Nigerian Dwarf',
            'Spanish',
            'Tennessee',
            'Boer',
            'Kiko',
            'Angora',
            'Cashmere',
            'Pygmy',
            'Fainting',
            'Nigerian Dwarf',
            'Alpine',
            'LaMancha'
        ]
    },
    {
        speciesName: 'Guinea Pig',
        breeds: [
            'American',
            'Abyssinian Guinea Pig',
            'Peruvian Guinea Pig',
            'Silkie Guinea Pig',
            'The Crested Guinea Pig',
            'The Teddy Guinea Pig',
            'Texel Guinea Pig',
            'Coronet Guinea Pig'
        ]
    },
    {
        speciesName: 'Hamster',
        breeds: [
            'Syrian Hamsters or Teddy Bear Hamster',
            'Dwarf Campbell Russian Hamsters',
            'Dwarf Winter White Russian Hamsters',
            'Roborovski Dwarf Hamsters',
            'Chinese Hamsters'
        ]
    },
    {
        speciesName: 'Hedgehog',
        breeds: [
            'European hedgehog',
            'Southern white-breasted hedgehog',
            'Northern white-breasted hedgehog',
            'Amur hedgehog',
            'Four-toed hedgehog',
            'North African hedgehog',
            'Somali hedgehog',
            'Southern African hedgehog',
            'Long-eared hedgehog',
            'Indian long-eared hedgehog',
            'Daurian hedgehog',
            'Hugh\'s hedgehog',
            'Desert hedgehog',
            'Indian hedgehog',
            'Brandt\'s hedgehog',
            'Bare-bellied hedgehog'
        ]
    },
    {
        speciesName: 'Hermit Crab',
        breeds: [
            'Coenobita cavipes',
            'Coenobita rugosus',
            'Paguristes pugil',
            'Cancellus typus',
            'Strigopagurus elongates'
        ]
    },
    {
        speciesName: 'Horse',
        breeds: [
            'Arabian',
            'Quarter Horse',
            'Thoroughbred',
            'Tennessee Walker',
            'Morgan',
            'Paint',
            'Appaloosa',
            'Miniature Horse',
            'Warmblood',
            'Andalusian'
        ]
    },
    {
        speciesName: 'Iguana',
        breeds: [
            'Green Iguana',
            'Red Iguana',
            'Desert Iguana',
            'Rhinoceros Iguana',
            'Angel Island Chuckwalla',
            'Grand Cayman Iguana',
            'Galapagos Land Iguana'
        ]
    },
    {
        speciesName: 'Llama/Alpaca',
        breeds: [
            'Suri alpaca',
            'Huacaya alpaca'
        ]
    },
    {
        speciesName: 'Mantis',
        breeds: [
            'Chinese Mantis',
            'Giant Asian Mantis',
            'Budwing Mantis',
            'African Mantis',
            'Ghost Mantis',
            'Chinese Mantis',
            'Orchid Mantis,',
            'Dead Leaf Mantis,',
            'Devils Flower Mantis',
            'Indian Flower Mantis,',
            'Spiny Flower Mantis',
            'European Mantis'
        ]
    },
    {
        speciesName: 'Mouse',
        breeds: ['NA']
    },
    {
        speciesName: 'Rats',
        breeds: ['NA']
    },
    {
        speciesName: 'Newt/Salamander',
        breeds: [
            'Axolotl Tiger Salamander',
            'Fire Belly Newt',
            'Eastern Newt'
        ]
    },
    {
        speciesName: 'Pig',
        breeds: [
            'Vietnamese Potbellied Pigs',
            'Juliana Pigs',
            'Guinea Hogs',
            'Ossabaw Island Pigs',
            'Kune Kune Pigs',
            'Mexican Yucatan Pigs'
        ]
    },
    {
        speciesName: 'Rabbit/Bunny/Hare',
        breeds: [
            'Mini Rex',
            'Holland Lop',
            'Dutch Lop',
            'Dwarf Hotot',
            'Mini Lop',
            'Mini Satin',
            'Netherland Dwarf',
            'Polish',
            'Lionhead',
            'Jersey Wooly',
            'Californian',
            'Harlequin',
            'Havana',
            'Standard',
            'Chinchilla',
            'Himalayan',
            'Angoras',
            'Belgian Hare',
            'English Spot',
            'Flemish Giant'
        ]
    },
    {
        speciesName: 'Sheep',
        breeds: [
            'Romney Sheep',
            'Lincoln Delaine',
            'Merinos Dorper',
            'Katahdin Hampshire',
            'Suffolk/Southdown'
        ]
    },
    {
        speciesName: 'Snake',
        breeds: [
            'Corn Snake',
            'California Kingsnake',
            'Rosy Boa',
            'Gopher Snake',
            'Ball Python',
            'Garter Snake',
            'Milk Snake',
            'Carpet Python',
            'Ball Python',
            'Green Tree Python',
            'California Kingsnake',
            'Gopher Snake',
            'Corn Snake',
            'Burmese Python',
            'Brazilian Rainbow Boa'
        ]
    },
    {
        speciesName: 'Spider',
        breeds: [
            'Mexican Redknee Tarantulas (Brachypelma Smithi)',
            'Chilean Rose Tarantulas (Grammostola Rosea)',
            'Costa Rican Zebra Tarantulas (Aphonoplema Seemani)',
            'Mexican Redleg Tarantulas (Brachypelma Emilia)',
            'Honduran Curly Hair Tarantulas (Brachypelma Albopilosum)',
            'Pink Zebra Beauty Tarantulas (Eupaleastrus Campestratus)',
            'Pink Toe Tarantulas (Avicularia Avicularia)',
            'Brazilian Black Tarantulas (Grammostola Pulchra)',
            'Mexican Red Rump Tarantulas (Brachypelma Vagans)',
            'Desert Blonde Tarantulas (Aphonopelma Chalcodes)'
        ]
    },
    {
        speciesName: 'Stick-bugs',
        breeds: [
            'Mantis',
            'Phasmids',
            'Madagascar',
            'Mealworm',
            'Leaf Insects',
            'Giant Prickly Stick Insect',
            'Rhinoceros Beetles',
            'Stag Beetle',
            'Dubia Roach',
            'Sphodromantis Lineola',
            'Malayan Jungle Nymph',
            'Annam Walking Stick',
            'Mealworm Beetles',
            'House Cricket',
            'Japanese Rhinoceros Beetle'
        ]
    },
    {
        speciesName: 'Sugar Glider',
        breeds: [
            'Albino',
            'Black Beauty',
            'Black Face Black Beauty',
            'Caramel',
            'Classic',
            'Cremeino',
            'Leucistic',
            'Mosaic'
        ]
    },
    {
        speciesName: 'Turtle/Tortoise',
        breeds: [
            'Red-Eared Slider (Trachemys Scripta Elegans)',
            'Eastern Box Turtle (Terrapene Carolina Carolina)',
            'Western Painted Turtle (Chrysemys Picta Bellii)',
            'Map Turtle (Graptemys Geographica)',
            'Wood Turtle (Glyptemys [Clemmys] Insculpta)'
        ]
    }
];

let petBreedsSeed = () => {
    data.forEach(data => {
        PetBreed.create(data);
    });
};

let runSeeds = () => {
    petBreedsSeed();
};

runSeeds();
