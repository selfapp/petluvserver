import dotenv from 'dotenv';
import FacebookTokenStrategy from 'passport-facebook-token';
import User from '../models/User';

module.exports = function(app, passport) {
	app.use(passport.initialize());
	app.use(passport.session());

	passport.serializeUser(function(user, done) {
  		done(null, user._id);
	});

	passport.deserializeUser(function(user, done) {
  		done(null, user);
	});

	passport.use('facebook-token', new FacebookTokenStrategy({
    	clientID: process.env.FACEBOOK_APP_ID,
    	clientSecret: process.env.FACEBOOK_APP_SECRET
  	}, function(accessToken, refreshToken, profile, done) {
			var email = profile.emails[0].value;
			let query;
			
			if (email === "" || email === undefined || email === null) {
				query = {
					'social.fb.id': profile.id
				};
			}else {
				query = { $or: [{ 'local.email': email }, { 'social.fb.email': email }, { 'social.gmail.email': email }] };
			}
	  		
  			User.findOne(query, (err, user) => {
				if (err) {
					return done(err);
				}
				if(user) {
					if(user.social.fb.id === profile.id) {
						return done(null, user);
					}else{
						user.social.fb.email = profile.emails[0].value;
						user.social.fb.token = accessToken;
						user.social.fb.id = profile.id;
						user.social.fb.photo = profile.photos[0].value || '';
						user.social.fb.userName = profile.displayName || '';

						user.save(err => {
							if (err) {
								return done(err);
							}else{
								return done(null, user);
							}
						});
					}
				}
				if (!user) {
					let user = new User();

					user.social.fb.email = profile.emails[0].value;
					user.social.fb.token = accessToken;
					user.social.fb.id = profile.id;
					user.social.fb.photo = profile.photos[0].value || '';
					user.social.fb.userName = profile.displayName || '';

					user.save(err => {
						if (err) {
							return done(err);
						}else{
							return done(null, user);
						}
					});
				}
			});
  		}
	));
};