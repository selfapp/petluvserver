import nodemailer from 'nodemailer';
import Email from 'email-templates';
import aws from 'aws-sdk';

// configure AWS SDK
// aws.config.loadFromPath(`${__dirname}/config.json`);
// console.log("dirname", __dirname);

const FROM = `"petLuvs" <noreply@petluvs.com>`;

let setup = () => {
    let transport = nodemailer.createTransport({
        // host: "smtp.mailtrap.io",
        // port: 2525,
        pool: true,
        host: 'smtp-relay.gmail.com',
        port: 456,
        secure: true,
        service: 'Gmail',
        auth: {
          user: process.env.GMAIL_ID,
          pass: process.env.GMAIL_PASS
        }
    });
    // let transport = nodemailer.createTransport({
    //     transport: 'ses', // loads nodemailer-ses-transport
    //     accessKeyId: process.env.AWS_ACCESS_KEY,
    //     secretAccessKey: process.env.AWS_SECRET_KEY
    // });

    return transport;
};

export let sendConfirmationEmail = (user) => {
    const transport = setup();
    const emailTemplater = new Email({
        views: {
            options: {
                extension: 'html',
                map: {
                    html: 'htmling'
                }
            }
        }
    });

    let confirmationLink = user.generateConfirmationUrl();

    emailTemplater.render('../src/templates/welcome', {confirmationLink})
        .then((template) => {
            const email = {
                FROM,
                to: user.local.email,
                subject: "Welcome to petLuvs",
                html: template
            };

            transport.sendMail(email, (err, info) => {
                console.log('err', err);
                console.log('info', info);
            });
        })
        .catch(console.error);
};

export let resetEmailConfirmation = (user) => {
    const transport = setup();

    const emailTemplater = new Email({
        views: {
            options: {
                extension: 'html',
                map: {
                    html: 'htmling'
                }
            }
        }
    });

    let confirmationLink = user.generateConfirmationUrl();

    emailTemplater.render('../src/templates/reset_pass', {confirmationLink})
        .then((template) => {
            const email = {
                FROM,
                to: user.local.email,
                subject: "Your password has been reset.",
                html: template
            };

            transport.sendMail(email, (err, info) => {
                console.log('err', err);
                console.log('info', info);
            });
        })
        .catch(console.error);
};
