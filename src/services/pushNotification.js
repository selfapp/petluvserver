/*
* Categories for notifications
* 1 - Do a review on Play date
* 2 - Review added to your Play date
* 3 - Service request
* 4 - Playdate created
* */
// Create a new Expo SDK client
import Expo from 'expo-server-sdk';
import moment from 'moment';

import Notification from '../models/Notification';
import Playdate from '../models/Playdate';

let expo = new Expo();

let sendNotification = (body, data, tokens) => {
    return new Promise((resolve, reject) => {
        let messages = [];

        // let pushToken = 'ExponentPushToken[VGt8LZLg5cjKL1IoZILMuo]';
        for (let pushToken of tokens) {
            if (!Expo.isExpoPushToken(pushToken)) {
                console.error('Push token' + pushToken + ' is not a valid Expo push token');
                resolve('error');
            }

            messages.push({
                to: pushToken,
                sound: 'default',
                body: body,
                data: data,
            });
        }

        let chunks = expo.chunkPushNotifications(messages);
        let tickets = [];
        (async () => {
            for (let chunk of chunks) {
                try {
                    let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
                    tickets.push(...ticketChunk);
                    resolve('success');
                } catch (error) {
                    reject('error');
                }
            }
        })();
    });
};

// Create the messages that you want to send to clents
export const pushNotification = (somePushTokens, message) => {
    return new Promise((resolve, reject) => {
        if(somePushTokens.length === 0){
            resolve();
        }
        let messages = [];

        for (let pushToken of somePushTokens) {
            if (!Expo.isExpoPushToken(pushToken)) {
                console.error('Push token' + pushToken + ' is not a valid Expo push token');
                resolve('error');
            }

            messages.push({
                to: pushToken,
                title: message.title,
                sound: 'default',
                body: message.body
            })
        }

        let chunks = expo.chunkPushNotifications(messages);
        let tickets = [];
        (async () => {
            for (let chunk of chunks) {
                try {
                    let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
                    console.log("tk", ticketChunk);
                    tickets.push(...ticketChunk);
                    resolve('success');
                } catch (error) {
                    resolve('error');
                }
            }
        })();
    });
};

export const reviewNotification = (id, somePushTokens, userId = null) => {
    Playdate.findOne({_id: id}, null, {
        populate: [
            {
                path: 'pets',
                select: 'imagePath name'
            }
        ]
    }).exec((err, playdate) => {
        let text = 'Tap to review the play date';
        let body = "Review the play date";
        let image;

        if(playdate.pets.length > 0) {
            if(playdate.pets[0].imagePath.length > 0) {
                image = playdate.pets[0].imagePath[0].url;
            }

            text = `Review ${playdate.pets[0].name}'s playdate`
        }

        let data = {
            text: text,
            id: id,
            category: 1
        };

        let notification = new Notification({
            body: body,
            category: 1,
            userId: userId,
            dataId: id,
            image: image
        });

        notification.save();
        return sendNotification(body, data, somePushTokens);
    });
};

export const reviewAddedNotification = (id, pushTokens, userId = null) => {
    let data = {
        id: id,
        category: 2
    };
    let body = "A new review has been added to your play date";
    let notification = new Notification({
        body: body,
        category: 2,
        userId: userId,
        dataId: id
    });
    notification.save();
    return sendNotification(body, data, pushTokens);
};

export const serviceRequestNotification = (id, tokens, string, popup) => {
    let notification = new Notification({
        body: string,
        category: 3
    });

    let data = {
        category: 3,
        popup,
        dataId: id,
        date: moment().format()
    };

    notification.save();
    return sendNotification(string, data, tokens);
};
