import async from 'async';
import log4js from 'log4js';

import Playdate from '../models/Playdate';
import User from '../models/User';
import parseErrors from "../utils/parseError";
import {reviewNotification, reviewAddedNotification} from './pushNotification';
import { queConn } from '../config/kuePortConfig';

const logger = log4js.getLogger();
let queue = queConn;

let playDateReview = () => {
  logger.level = 'info';

  queue.process('playDate-review', (job, done) => {
    logger.info(`*******Processing playdateReview kue******`);
    logger.info(`Processing job = ${job.id} with data = ${JSON.stringify(job.data)}`);

    Playdate.findOne({ _id: job.data.id }).exec((err, playdate) => {
      if (err) done(new Error(JSON.stringify(parseErrors(err.errors))));
      let userIds = playdate.userJoining;

      if (userIds.length > 0) {
        User.find({ _id: { $in : userIds }, pushToken: {$ne: null}}).select('pushToken').sort({  updatedAt: -1}).exec((err, userTokens) => {
          if (err) done(new Error(JSON.stringify(parseErrors(err.errors))));
          let tokenArr = userTokens.map(userTk => userTk.pushToken );
          reviewNotification(playdate._id, tokenArr, playdate.userId);

          logger.info(`*******Successfully completed job with id = ${job.id} ******`);
        })
      }
    })
  })
};

let playDateReviewAdded = () => {
  queue.process('playDate-review-added', (job, done) => {
    logger.info(`Processing playDate-review-added kue`);

    Playdate.findOne({_id: job.data.playDateId})
      .exec((err, playDate) => {
        if (err) {
          logger.error(`Error while find a playdate with id ${job.data.playDateId}`);
          done(new Error(err));
        }

        if (!playDate) {
          logger.error(`Could not found a playdate ${job.data.playDateId}`);
          done(new Error(`Not able to find playdate ${job.data.playDateId}`));
        }

        if (playDate) {
          User.findOne({_id: playDate.userId})
          .exec((err, user) => {
            if (err) done(err);

            if (user) {
              reviewAddedNotification(job.data.playDateId, [user.pushToken], user._id)
                .then((status) => {
                  done();
                })
                .catch((err) => {
                  done(err);
                });
            }
          });
        }
      });
  });
};

export default function processJobs() {
  playDateReview();
  playDateReviewAdded();
};
