import express from 'express';
import Notification from '../models/Notification';
import log4js from 'log4js';
import moment from 'moment';
import chalk from 'chalk';

import authenticate from "../middlewares/authenticate";
import User from "../models/User";
import Pet from "../models/Pet";
import Playdate from "../models/Playdate";
import {pushNotification, reviewNotification} from '../services/pushNotification';

import { queConn } from '../config/kuePortConfig';
import processJobs from '../services/jobs';
let queue = queConn;

const logger = log4js.getLogger();
const router = express.Router();
router.use(authenticate);

const PLAYDATE_ID = '5d4764a737d4d35d508228b1';

router.get("/getAllPushTokens", async (req, res) => {
    const pushTokens = await User.find({pushToken: {$ne: null}});

    let tokenArr = pushTokens.map(el => {
        if (el.pushToken)
        return el['pushToken']
    });
    return res.status(200).json({ response: tokenArr });
})

router.post("/sendNotifications", async (req, res) => {
    let tokenArr = req.body.tokens;

    let user = await User.findOneAndUpdate(
        { _id: "5d296617e851182e8b24e7be" }, 
        { $push: { reviews: '5d296617e851182e8b24e7be' } }
      );

    console.log(user);

    // const userTokens = await User.find({pushToken: {$ne: null} }).select('pushToken').sort({  updatedAt: -1});
    // let tokenArr = userTokens.map(userTk => userTk.pushToken );
    // console.log(tokenArr);



    // let users = await User.find({_id: playDate.userJoining,pushToken: {$ne: null} })
    // console.log(users);
    // let message = {
    //     "title": "New Playdate!",
    //     "body": `A new playdate has been created by ${pet.name.toUpperCase()}. Playdate will be at ${playdate.place}`
    //   }

    // let notRes = await pushNotification(tokenArr, message);

    return res.status(200).json({ response: 'success' });
})

router.get("/processJobs", (req, res) => {
    processJobs();
    return res.status(200).json({ success: true });
})

router.post("/createjob", (req,res) => {
    Playdate.findOne({ _id: PLAYDATE_ID }).exec((err, playdate) => {
        let job = queue.create('playDate-review', {
            id: playdate._id
          })
            .delay(60000)
            .save((err) => {
                if (err) {
                    console.log(err);
                    return res.status(400).json({ error: err });
                }
              if (!err) console.log(job.id);
              return res.status(400).json({ jobId: job.id });
         });
    })
})

export default router;