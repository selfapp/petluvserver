# PetLuvs API

- [Auth](#auth)
    - [Sign Up](#sign-up)
    - [Sign In](#sign-in)
- [Users](#users)
    - [Profile](#profile)
- [Reviews](#reviews)
    - [Create Review](#create-review)
    - [Get Reviews](#get-reviews)
    - [Accept Review](#accept-review)
    - [Decline Review](#decline-review)
    - [My Reviews](#my-reviews)
    - [Share Review](#share-review)
- [Pets](#pets)
    - [New](#new-pet)
- [Playdate](#playdates)
    - [New](#new-playdate)
- [Notifications](#notifications)
    - [Get All](#get-all)
    - [Remove All](#remove-all)
    - [Mark Read](#mark-read)
- [Places](#places)
    - [Search from Google](#search-from-google)

    let baseUrl = "http://localhost:3001/api" 

 
### Auth

#### Sign-up
##### URL: `{{ baseUrl }}/auth/signup`
##### Method: `POST`
##### Body:

    {
      "email": "p@p.com",
      "password": "123456",
      "passwordConfirmation": "123456"
    }
    
##### Response(OK):

    {
      "user": {
        "token": "TOKEN"
      }
    }
    
#### Sign-in
##### URL: `{{ baseUrl }}/auth/signin`
##### Method: `POST`
##### Body:

    {
      "email": "a@a.com",
      "password": "123456"
    }

##### Response(if sign-in okay):

    {
      "user": {
        "token": "TOKEN"
      }
    }
    
##### Response(if sign-in not okay):

    {
      "errors": [
        {
          "global": "invalid Credentials"
        }
      ]
    }

### Users

#### Profile
##### URL: `{{ baseUrl }}/users/profile`
##### Method: `GET`
##### Body:

    {
      "local": {
        "confirmed": false,
        "confirmationToken": "TOKEN",
        "forgotPasswordToken": null,
        "email": "p@p.com"
      },
      "currentLocation": {
        "lat": null,
        "lng": null
      },
      "accountType": {
        "breeder": {
          "isServiceProvider": false
        },
        "petSitter": {
          "isServiceProvider": false
        },
        "petGroomer": {
          "isServiceProvider": false
        },
        "petTrainer": {
          "isServiceProvider": false
        },
        "fosterPets": {
          "isServiceProvider": false
        },
        "rescueAnimals": {
          "isServiceProvider": false
        }
      },
      "pushToken": null,
      "enableServices": [
        "SITTER",
        "GROMMER",
        "TRAINER",
        "FOSTER_PETS",
        "RESCUE_ANIMALS"
      ],
      "pets": [],
      "playdates": [],
      "_id": "123123123",
      "createdAt": "2019-03-15T09:50:36.151Z",
      "updatedAt": "2019-03-15T09:50:36.151Z",
      "__v": 0
    }

### Reviews

#### Create Review
##### URL: `{{ baseUrl }}/reviews/:playdate_id`
##### Method: `POST`
##### Body: 

    {
      "starCount": 4,
      "description": "This is a demo"
    }

##### Response:

    {
      "review": {
        "starCount": 5,
        "reviewFor": "playdate",
        "verified": false,
        "_id": "5c8b8901f8559a0c02813615",
        "description": "This is amazing",
        "userId": "5c8b756bf62f91087152525f",
        "playdateId": "5c8b7d33362ca5092b1bda8b",
        "createdAt": "2019-03-15T11:14:09.431Z",
        "updatedAt": "2019-03-15T11:14:09.431Z",
        "__v": 0
      }
    }
    
#### Get Reviews
##### URL: `{{ baseUrl }}/reviews/:playdate_id`
##### Method: `GET`
##### Response: 

    {
      "reviews": [
        {
          "starCount": 5,
          "reviewFor": "playdate",
          "verified": false,
          "_id": "5c8b8901f8559a0c02813615",
          "description": "This is amazing",
          "userId": "5c8b756bf62f91087152525f",
          "playdateId": "5c8b7d33362ca5092b1bda8b",
          "createdAt": "2019-03-15T11:14:09.431Z",
          "updatedAt": "2019-03-15T11:14:09.431Z",
          "__v": 0
        }
      ]
    }

#### Accept Review
##### URL: `{{ baseUrl }}/reviews/accept/:review_id`
##### Method: `POST`
##### Response: 

    {
      "review": {
        "starCount": 1,
        "reviewFor": "playdate",
        "verified": true,
        "_id": "5c9b0d8815703d9d52691a0c",
        "userId": "5c8b756bf62f91087152525f",
        "playdateId": "5c8b7d33362ca5092b1bda8b",
        "createdAt": "2019-03-27T05:43:36.006Z",
        "updatedAt": "2019-03-27T05:57:57.676Z",
        "__v": 0
      }
    }
    
#### Decline Review
##### URL: `{{ baseUrl }}/reviews/decline/:review_id`
##### Method: `DELETE`
##### Response(if review not accepted before):

    {
      "status": true
    }

##### Response(if review already accepted):

    {
      "status": false,
      "message": "Can't remove accepted review"
    }
    
#### My Reviews
##### URL: `{{ baseUrl }}/reviews`
##### Method: `GET`
##### Response:

     {
       "reviews": [
         {
           "starCount": 5,
           "reviewFor": "playdate",
           "verified": true,
           "_id": "5c9e00352d4de18c6b0f4a6b",
           "description": "ghhg",
           "userId": {
             "local": {
               "email": "s@s.com"
             },
             "_id": "5c99b73dae39e36f653057e4"
           },
           "playdateId": "5c9dff65ad96928c2b70fd2f",
           "createdAt": "2019-03-29T11:23:33.865Z",
           "updatedAt": "2019-03-29T11:26:37.020Z",
           "__v": 0
         }
       ],
       "avgStarCount": 5
     }
     
#### Share Review
##### URL: `{{ baseUrl }}/reviews/share/:id`
##### Method: `POST`
##### Response(if review found and verified):

    {
      "status": true
    } 
    
##### Response(if review not found or review not verified):

    {
      "status": false,
      "error": "Review is not valid"
    }

### Pets

#### New Pet
##### URL: `{{ baseUrl }}/pets`
##### Method: `POST`
##### Body:

    {
    	"name": "DJay",
    	"species": "GSD",
    	"breed": "GSD",
    	"birthday": "2009/10/30",
    	"insured": true,
    	"fixed": true,
    	"fullyVacinated": true
    }

### Playdates

#### New Playdate
##### URL: `{{ baseUrl }}/playdates`
##### Method: `POST`
##### Body:

    {
    	"pets": [
    		"5c8b79f7362ca5092b1bda87"
    	],
    	"place": "Pawliday Inn Pet Resort",
    	"description": "Play for DJay",
    	"star": 0,
    	"startTime": "2019/03/20",
    	"endTime": "2019/03/21 16:00:00",
    	"status": "public",
    	"nofityFriends": false	
    }
    
##### Response:

    {
      "playdate": {
        "pets": [
          {
            "_id": "5c8b79f7362ca5092b1bda87",
            "name": "DJay",
            "imagePath": [
              {
                "imageName": "",
                "url": ""
              }
            ]
          }
        ],
        "nofityFriends": false,
        "userJoining": [],
        "reviews": [],
        "_id": "5c9222c5f58883d5079b9147",
        "userId": {
          "local": {
            "email": "p@p.com"
          },
          "currentLocation": {
            "lat": 28.515058392034,
            "lng": 77.08039285187606
          },
          "_id": "5c8b756bf62f91087152525f"
        },
        "place": "Pawliday Inn Pet Resort",
        "startTime": "2019-03-19T18:30:00.000Z",
        "endTime": "2019-03-21T10:30:00.000Z",
        "status": "public",
        "location": {
          "photos": [
            "https://fastly.4sqi.net/img/general/1333x2000/76335900_PbwwJHEze689qlw7xuh7fzQ4IiyuO-lmXBxiHDfyacU.jpg",
            "https://fastly.4sqi.net/img/general/640x852/76335900_RwF9vkqsJXpOWVAVvpkn9hlTq5yfC7MdF4MzW_ChFAY.jpg"
          ],
          "_id": "5c9dbde851c5c5790a54df9f",
          "description": "Pawliday Inn is the perfect place to leave your pets while at work or vacation! We offer services such as grooming, chauffeurs, daycare, and boarding.",
          "star": 2,
          "hours": "Closed until 7:00 AM Monday",
          "locName": "Pawliday Inn Pet Resort",
          "createdAt": "2019-03-29T06:40:40.945Z",
          "updatedAt": "2019-03-29T06:40:40.945Z",
          "__v": 0
        },
        "createdAt": "2019-03-20T11:23:49.094Z",
        "updatedAt": "2019-03-20T11:23:49.094Z",
        "__v": 0
      }
    }

### Notifications

#### Get All
##### URL: `{{ baseUrl }}/notifications`
##### Method: `GET`
##### Response:

    {
      "notifications": [
        {
          "read": false,
          "_id": "5ca2e55061d737075a663d42",
          "body": "Review the play date",
          "category": 1,
          "userId": "5c8b756bf62f91087152525f",
          "createdAt": "2019-04-02T04:30:08.070Z",
          "updatedAt": "2019-04-02T04:30:08.070Z",
          "__v": 0
        }
      ]
    }

#### Remove All
##### URL: `{{ baseUrl }}/notifications`
##### Method: `DELETE`
##### Response:

    {
      "status": true
    }


#### Mark Read
##### URL: `{{ baseUrl }}/notifications/read/:id`
##### Method: `POST`
##### Response(if notification ID was correct):

    {
        "status": true
    }
    
##### Response(if notification ID is not correct):

    {
        "status": false
    }

### Places

#### Search from Google
##### URL: `{{ baseUrl }}/places/place-suggestions-google`
##### Method: `GET`
##### Params:
    query
    

This should be a "+" separated string. If you want to search for "New Orleans", you will give "New+Orleans" in `query` params. Also query param
should have lat and lng for user current location. eg.{{ baseUrl }}/places/place-suggestions-google?searchKey=dogpark&lat=32.78260925963876"&lng=-96.79933139703398. Make sure to get user current location from the app.

##### Response:

    {
      "success": true,
      "msg": "Successfully Retrived data from google.",
      "response": {
        "data": [
          {
            "name": "DLF Cyber City",
            "place_id": "ChIJ0deFKTgZDTkRsvLmX8btO7A",
            "formatted_address": "DLF Cyber City, DLF Phase 2, Sector 24, Gurugram, Haryana 122022, India"
          }
        ]
      }
    }
