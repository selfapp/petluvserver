FROM node:10 as builder
RUN mkdir -p /usr/app
WORKDIR /usr/app
COPY ./package.json /usr/app
RUN npm install 
COPY . /usr/app

EXPOSE 3000
CMD [ "npm", "run", "prod" ]

# FROM nginx
# COPY --from=builder /usr/app/dist /usr/share/nginx/html
