const shell = require('webadnan-npm-shell');
const AuthTest = require('./test/auth-test');
const BASE_URL = 'http://localhost:3001';

(async () => {
  let testFiles = await shell('ls test/*-test.js');
  testFiles = testFiles.trim().split('\n');
  let totTest = 0, totFail = 0;
  for (let f=0; f<testFiles.length; f++) {
    let file = testFiles[f];
    let TestClass = require('./' + file);
    let testObj = new TestClass(BASE_URL);
    let testMethods = Object.getOwnPropertyNames(TestClass.prototype)
      .filter(method => method.indexOf('test') == 0);
    for (let i=0; i<testMethods.length; i++) {
      await testObj.baseSetup();
      await testObj.setup();
      let method = testMethods[i];
      testObj.log(`${testObj.name()}.${method}() - started...`, 'green');
      let testResult = await testObj.runMethod(method);
      let testResultText = testResult ? 'SUCCESS' : 'FAIL';
      totTest++;
      if (!testResult) {
        totFail++;
        console.log(testObj.getErrorMsg());
        testObj.log(`${testObj.name()}.${method}() - ${testResultText}`, 'red');
      } else {
        testObj.log(`${testObj.name()}.${method}() - ${testResultText}`, 'green');
      }
      await testObj.tearDown();
      await testObj.baseTearDown();
      console.log('\n\n');
    }
  }
  console.log(' ');
  console.log(`Total test : ${totTest}`);
  console.log(`SUCCESS    : ${totTest - totFail}`);
  console.log(`FAIL       : ${totFail}`);
})();
