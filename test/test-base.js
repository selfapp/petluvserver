const shell = require('webadnan-npm-shell');

module.exports = class {
  constructor(baseUrl) {
    this.baseUrl = baseUrl;
    this.errorCount = 0;
    this.errorMsgs = [];
  }

  name() {
    // Subclass must need to implement.
    throw new Error('Implement the method name()');
  }

  async baseSetup() {
    // Subclass may implement.
    return new Promise(async resolve => {
      this.errorCount = 0;
      this.errorMsgs = [];
      resolve(true);
    })
  }

  async setup() {
    // Subclass may implement.
    return new Promise(async resolve => {
      resolve(true);
    })
  }

  async baseTearDown() {
    // Subclass may implement.
    return new Promise(async resolve => {
      resolve(true);
    })
  }

  async tearDown() {
    // Subclass may implement.
    return new Promise(async resolve => {
      resolve(true);
    })
  }

  async requestWithBody(url, body) {
    return new Promise(async resolve => {
      try {
        let bodyText = JSON.stringify(body);
        let cmd = `curl -s POST -H 'Content-Type: application/json' -d '${bodyText}' ${this.baseUrl}${url}`
        console.log('Request:');
        console.log(cmd);
        console.log();
        let output = await shell(cmd);
        let json = JSON.parse(output);
        console.log('Response:');
        console.log(json);
        console.log();
        resolve(json);
      } catch (e) {
        resolve({});
      }
    })
  }

  assert(flag, msg) {
    if (!flag) {
      this.errorMsg.push(msg);
      throw(msg);
    }
  }

  assertEqual(value, expected, msg) {
    if (value != expected) {
      let errorMsg = `Expected ${expected}, received ${value}`;
      this.errorMsgs.push(errorMsg);
      throw(msg || errorMsg);
    }
  }

  voidPromise(callback) {
    return new Promise(async (resolve, reject) => {
      try {
        await callback();
        resolve(true);
      } catch (e) {
        resolve(false);
      }
    });
  }

  // @final, Subclass should not override this
  async runMethod(method) {
    let self = this;
    return new Promise(async resolve => {
      let result = await self[method]();
      resolve(result);
    })
  }

  getErrorMsg() {
    return this.errorMsgs;
  }

  /**
  * Simple way to color the text with console log.
  * https://coderwall.com/p/yphywg/printing-colorful-text-in-terminal-when-run-node-js-script
  */
  log(msg, color) {
    if (color == 'red') {
      console.log('\x1b[31m%s\x1b[0m', msg);
    } else if (color == 'green') {
      console.log('\x1b[32m%s\x1b[0m', msg);
    } else {
      console.log(msg);
    }
  }
}
