const TestBase = require('./test-base');

module.exports = class extends TestBase {
  constructor(baseUrl) {
    super(baseUrl);
  }

  name() {
    return "AuthTest";
  }

  async testSignupWithInvalidEmail() {
    return this.voidPromise(async () => {
      let json = await this.requestWithBody('/api/auth/signup', {
        email: "webadnangmail.com",
        password: "webadnan123",
        passwordConfirmation: "webadnan123"
      });
      this.assert(json.errors[0].email == 'must be an email',
        'Invalid email msg is not matching.');
    })
  }

  async testSignupWithInvalidPassword() {
    return this.voidPromise(async () => {
      let json = await this.requestWithBody('/api/auth/signup', {
        email: "webadnan@gmail.com",
        password: "webad",
        passwordConfirmation: "webad"
      });
      this.assert(json.errors[0].password ==
            'passwords must be at least 6 chars long and contain one number',
        'Invalid password msg is not matching.');
    })
  }

  async testSignupWithDifferentPasswordAndConfirmPassword() {
    return this.voidPromise(async () => {
      let json = await this.requestWithBody('/api/auth/signup', {
        email: "webadnan@gmail.com",
        password: "webadnan123",
        passwordConfirmation: "webadnan124"
      });
      this.assert(json.errors[0].passwordConfirmation ==
            'passwordConfirmation field must have the same value as the password field',
        'Invalid password msg is not matching.');
    })
  }

  async testCreateUser() {
    return this.voidPromise(async () => {
      let json = await this.requestWithBody('/api/auth/signup', {
        email: "webadnan@gmail.com",
        password: "webadnan123",
        passwordConfirmation: "webadnan123"
      });
      this.assert(json.token.length > 0, 'signup should return some token.');
    })
  }

  async testSignin() {
    return this.voidPromise(async () => {
      let json = await this.requestWithBody('/api/auth/signin', {
        email: "webadnan@gmail.com",
        password: "webadnan123"
      });
      let token = json.user.token;
      this.assert(token.length > 0, 'signin should return some token.');
    })
  }

  async testDeleteUser() {
    return this.voidPromise(async () => {
      let json = await this.requestWithBody('/api/auth/delete-user', {
        email: "webadnan@gmail.com",
        password: "webadnan123"
      });
    })
  }

  async testSigninShouldFailAfterDeletingUser() {
    return this.voidPromise(async () => {
      let json = await this.requestWithBody('/api/auth/signin', {
        email: "webadnan@gmail.com",
        password: "webadnan123"
      });
      this.assertEqual(JSON.stringify(json), `{"errors":[{"global":"invalid Credentials"}]}`);
    })
  }
}
