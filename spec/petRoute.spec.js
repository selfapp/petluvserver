import request from 'request-promise';
import server from '../src/server';

let token = 'Bearer ';
const url = 'http://localhost:3001/api/pets';
// const token = `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViOGFjOThiZTRmZWY5NTJiMjY4NTkxZCIsImNvbmZpcm1lZCI6ZmFsc2UsImlhdCI6MTUzNTgyMjIxOX0.g2WHKV38PozNDH_XgeRbNWc-keePtIkS5UBhWVqGsp0`;

const newUser = {
    'email': 'testuser@gmail.com',
    'password': 'test3318',
    'passwordConfirmation': 'test3318'
};


describe('pets', function () {

    let signup = async () => {
        let res = await request.post(`http://localhost:3001/api/auth/signup`, {json: true, body: newUser});
        token = token + res.token;
    };

    beforeAll(() => {
        signup();
    });

    afterAll(() => {
        server.close();
    }); 

    it('should return 200 response code', done =>  {
        console.log(token);
        let options = {
            url,
            headers: {
                'Authorization': token
            }
        };
        request.get(options, (error, response, body) => {
            console.log(body);
            expect(response.statusCode).toEqual(200);
            done();
        });
    });

    // it('should fail on POST', function (done) {
    //     request.post(endpoint, {json: true, body: {}}, function (error, response) {
    //         expect(response.statusCode).toEqual(404);
    //         done();
    //     });
    // });

    // var server;
    // beforeAll(() => {
    //     server = require("../app");
    // });
    // afterAll(() => {
    //     server.close();
    // });
    // var data = {};
    // beforeAll((done) => {
    //     request.get(options, (error, response, body) => {
    //         data.status = response.statusCode;
    //         data.body = body;
    //         console.log(body);
    //         done();
    //     });
    // });
});