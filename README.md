# petLuvs-api


#API Routes:
	Signin -> /api/auth/signin
	Signup -> /api/auth/signup
	Facebook Login -> /api/auth/facebook/token
	Instragram Login -> Yet to implement


# To run the app. Need to follow following steps
	Step1 - make sure to run redis server
		- Need to install redis first: 
			command: redis-server
	Step2 - Make sure to install dependencies 
		- In your package.json level
			command: npm install
	Step3 - Run the app
		- command: npm run dev


#To Connect to mongodb database from commandline:
	mongo "mongodb+srv://petluvsdev:petluvsdev3318@petluvs-lku9c.mongodb.net/petluvs?retryWrites=true"


#Elestic Beanstalk deployment
	Steps:
		- First build the app. This will create new dist folder
			npm run build
		- Second: eb deploy -l <archiveName>
			eg. eb deploy -l petluvs-api_19_07_05-03:52