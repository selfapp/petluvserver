import mongoose from 'mongoose';
import axios from 'axios';

import Location from '../src/models/Location';

const MONGODB_DEV_URL="mongodb+srv://petluvsdev:petluvsdev3318@petluvs-lku9c.mongodb.net/petluvs?retryWrites=true"
const GOOGLE_PLACE_DETAILS_BASE_URL = "https://maps.googleapis.com/maps/api/place/details/json";
const GOOGLE_API_KEY = "AIzaSyDvuLYaMYX5Qn7uEmsVe1d7vG7oPXGWsA4";
const GOOGLE_PHOTOS_BASE_URL = "https://maps.googleapis.com/maps/api/place/photo";

mongoose.Promise = global.Promise;
mongoose.connect(MONGODB_DEV_URL)
	.then(async () => {
        console.log('connected to mongodb');
		let place = await Location.findOne({_id : "5cec527244a610b4096582fe"});
		let placeId = place.placeId;

		let placeObj = {
			placeId: "",
			category: "",
			photos: [],
			description: "",
			star: 0,
			hours: [],
			locName: "",
			provider: "google",
			phoneNumber: "",
			address: "",
			reviews: null,
			types: null
		  };
		
		const queryParams = `?placeid=${placeId}&key=${GOOGLE_API_KEY}&fields=name,rating,formatted_phone_number,opening_hours,formatted_address,website,photo,review,place_id,type,geometry`;
		const url = `${GOOGLE_PLACE_DETAILS_BASE_URL}${queryParams}`;

		try {
			let googleRes = await axios.get(url);
			let response = googleRes['data'];

			if (response['status'] === 'OK') {
				let data = response["result"];
				place['phoneNumber'] = data['formatted_phone_number'] ? data['formatted_phone_number'] : "";
				place['address'] = data['formatted_address'];
				place['star'] = data['rating'] ? data['rating'] : 0;
				place['website'] = data['website'] ? data['website'] : "";
				place['locName'] = data['name'];
				place['hours'] = data['opening_hours'] ? data['opening_hours']['weekday_text'] : null;
				place['isOpen'] = data['opening_hours'] ? data['opening_hours']['open_now'] : false;
				place['types'] = data['types'];
				place['lat'] = data['geometry']['location']['lat'];
				place['lng'] = data['geometry']['location']['lng'];
		
				const location = { type: 'Point', coordinates: [place['lng'], place['lat']] };
				Location['location'] = location;
		
				if (data['photos']) {
				  place['photos'] = []
				  let firstThreePhotos = data['photos'].slice(0, 3);
				  let photos = firstThreePhotos.map(p => {
					let placeUrl = `${GOOGLE_PHOTOS_BASE_URL}?key=${process.env.GOOGLE_API_KEY}&photoreference=${p.photo_reference}&maxwidth=400`;
					return placeUrl;
				  });
				  place['photos'] = [...place['photos'], ...photos];
				}
		
				if (data['reviews']) {
				  place['reviews'] = []
				  let firstThreeReviews = data['reviews'].slice(0, 3);
				  place['reviews'] = firstThreeReviews;
				}

				let updatedPlace = await place.save();
				console.log(updatedPlace);
			}
		} catch (e) {
			console.log(e);
			console.log("error while fetching google data.")
		}
			
    })
	.catch(err => {
		console.log(err);
		console.log('Could Not connected to database');
	});